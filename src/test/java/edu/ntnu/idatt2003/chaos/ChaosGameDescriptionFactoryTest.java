package edu.ntnu.idatt2003.chaos;

import edu.ntnu.idatt2003.model.chaos.ChaosGameDescription;
import edu.ntnu.idatt2003.model.chaos.ChaosGameDescriptionFactory;
import edu.ntnu.idatt2003.model.math.Vector2D;
import edu.ntnu.idatt2003.model.transformations.AffineTransform2D;
import edu.ntnu.idatt2003.model.transformations.JuliaTransform;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

class ChaosGameDescriptionFactoryTest {
  private ChaosGameDescription juliaDesc;
  private ChaosGameDescription barnsleyDesc;
  private ChaosGameDescription sierpinskiDesc;

  @BeforeEach
  void setUp() {
    try {
      // Gets to test ChaosGameDescriptionFactory.get through the positive and negative tests below.
      juliaDesc = ChaosGameDescriptionFactory.get("Julia");
      barnsleyDesc = ChaosGameDescriptionFactory.get("Barnsley");
      sierpinskiDesc = ChaosGameDescriptionFactory.get("Sierpinski");

    } catch (Exception e) {
      fail("Something went wrong while creating chaos game description: " + e.getMessage());
    }
  }

  @Nested
  @DisplayName("Positive Tests")
  class PositiveTests {

    @Test
    @DisplayName("Should create Julia ChaosGameDescription from corresponding vectors and transformations")
    void testCreateJulia() {
      assertNotNull(juliaDesc);

      assertEquals(new Vector2D(-1.6, -1), juliaDesc.getMinCoords());
      assertEquals(new Vector2D(1.6, 1), juliaDesc.getMaxCoords());

      assertEquals(2, juliaDesc.getTransforms().size());
      assertTrue(juliaDesc.getTransforms().get(0) instanceof JuliaTransform);
      assertTrue(juliaDesc.getTransforms().get(1) instanceof JuliaTransform);
    }

    @Test
    @DisplayName("Should create Barnsley ChaosGameDescription from corresponding vectors and transformations")
    void testCreateBarnsley() {
      assertNotNull(barnsleyDesc);

      assertEquals(new Vector2D(-2.65, 0.0), barnsleyDesc.getMinCoords());
      assertEquals(new Vector2D(2.65, 10.0), barnsleyDesc.getMaxCoords());

      assertEquals(4, barnsleyDesc.getTransforms().size());
      assertTrue(barnsleyDesc.getTransforms().get(0) instanceof AffineTransform2D);
      assertTrue(barnsleyDesc.getTransforms().get(1) instanceof AffineTransform2D);
      assertTrue(barnsleyDesc.getTransforms().get(2) instanceof AffineTransform2D);
      assertTrue(barnsleyDesc.getTransforms().get(3) instanceof AffineTransform2D);
    }

    @Test
    @DisplayName("Should create Sierpinski ChaosGameDescription from corresponding vectors and transformations")
    void testCreateSierpinski() {
      assertNotNull(sierpinskiDesc);

      assertEquals(new Vector2D(0.0, 0.0), sierpinskiDesc.getMinCoords());
      assertEquals(new Vector2D(1.0, 1.0), sierpinskiDesc.getMaxCoords());

      assertEquals(3, sierpinskiDesc.getTransforms().size());
      assertTrue(sierpinskiDesc.getTransforms().get(0) instanceof AffineTransform2D);
      assertTrue(sierpinskiDesc.getTransforms().get(1) instanceof AffineTransform2D);
      assertTrue(sierpinskiDesc.getTransforms().get(2) instanceof AffineTransform2D);
    }

    @Test
    @DisplayName("Should not throw IllegalArgumentException for type in different uppercase and lowercase.")
    void testDifferentCaseType() {
      assertDoesNotThrow(() -> ChaosGameDescriptionFactory.get("JulIa"));
      assertDoesNotThrow(() -> ChaosGameDescriptionFactory.get("BARNSLEy"));
      assertDoesNotThrow(() -> ChaosGameDescriptionFactory.get("sierPINSKi"));
    }
  }

  @Nested
  @DisplayName("Negative Tests")
  class NegativeTests {

    @Test
    @DisplayName("Should throw IllegalArgumentException for unknown type")
    void testUnknownType() {
      assertThrows(IllegalArgumentException.class, () -> ChaosGameDescriptionFactory.get("unknown"));
    }

    @Test
    @DisplayName("Should throw NullPointerException for type null")
    void testNullType() {
      assertThrows(NullPointerException.class, () -> ChaosGameDescriptionFactory.get(null));
    }

    @Test
    @DisplayName("Should throw IllegalArgumentException for empty string")
    void testEmptyType() {
      assertThrows(IllegalArgumentException.class, () -> ChaosGameDescriptionFactory.get(""));
    }
  }
}
