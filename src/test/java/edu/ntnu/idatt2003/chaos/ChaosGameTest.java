package edu.ntnu.idatt2003.chaos;

import edu.ntnu.idatt2003.model.chaos.ChaosGameDescription;
import edu.ntnu.idatt2003.model.math.Matrix2x2;
import edu.ntnu.idatt2003.model.math.Vector2D;
import edu.ntnu.idatt2003.model.transformations.AffineTransform2D;
import edu.ntnu.idatt2003.model.chaos.ChaosGame;
import edu.ntnu.idatt2003.model.transformations.Transform2D;
import org.junit.jupiter.api.*;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;


class ChaosGameTest {
  private ChaosGame chaosGame;
  private ChaosGameDescription description;
  private int width;
  private int height;
  Vector2D minCoords;
  Vector2D maxCoords;
  List<Transform2D> transforms;
  Matrix2x2 matrix;
  Vector2D vector2D;


  @BeforeEach
  void setUp() {
    try {
      width = 100;
      height = 100;

      minCoords = new Vector2D(0, 0);
      maxCoords = new Vector2D(1, 1);

      matrix = new Matrix2x2(0.5, 0, 0, 0.5);
      vector2D = new Vector2D(0, 0);
      transforms = new ArrayList<>();
      transforms.add(new AffineTransform2D(matrix, vector2D));

      description = new ChaosGameDescription(transforms, minCoords, maxCoords);
      chaosGame = new ChaosGame(description, width, height);
    } catch (Exception e) {
      fail("Something went wrong while setting up test data: " + e.getMessage());
    }

  }


  @Nested
  @DisplayName("Positive tests for AffineTransform2D.")
  class PositiveTests {

    @Test
    @DisplayName("Positive test for checking if the canvas is the right size.")
    void testCanvasSize() {
      int[][] canvasAsArray = chaosGame.getCanvas().getCanvasArray();
      assertEquals(width, canvasAsArray.length);
      assertEquals(height, canvasAsArray[0].length);
    }

    @Test
    @DisplayName("ChaosGame should not throw exception when running runSteps method correctly.")
    void testRunStepsWithoutException() {
      assertDoesNotThrow(() -> chaosGame.runSteps(1000));
    }

    @Test
    @DisplayName("ChaosCanvas should not be null.")
    void testChaosCanvasNotNull() {
      assertNotNull(chaosGame.getCanvas());
    }

    @Test
    @DisplayName("ChaosGame should not be null.")
    void testChaosGameNotNull() {
      assertNotNull(chaosGame);
    }


    @Test
    @DisplayName("Random point in the canvas changes when the runSteps method runs.")
    void testRandomPointChangesWithRunSteps() {
      try {
        ChaosGame chaosGame2 = new ChaosGame(description, width, height);
        chaosGame2.runSteps(1000);

        chaosGame.runSteps(1000);

        assertNotEquals(chaosGame.getCanvas(), chaosGame2.getCanvas());

      } catch (Exception e) {
        fail("Exception was thrown: " + e.getMessage());
      }
    }

    @Test
    @DisplayName("Constructor for ChaosGame does not throw exceptions with valid values.")
    void testValidConstructor() {
      try {
        assertDoesNotThrow(() -> new ChaosGame(description, 200, 200));
      } catch (Exception e) {
        fail("Exception was thrown: " + e.getMessage());
      }
    }
  }

  @Nested
  @DisplayName("Negative Tests ChaosGame. Should throw exceptions on invalid input or values.")
  class NegativeTest {
    @Test
    @DisplayName("Should throw IllegalArgumentException on negative number for amount of steps.")
    void runStepsNegativeNumber() {
      Assertions.assertThrows(IllegalArgumentException.class, () -> chaosGame.runSteps(-14));
    }

    @Test
    @DisplayName("Should throw IllegalArgumentException if description is null.")
    void constructorWithDescriptionAsNull() {
      Assertions.assertThrows(NullPointerException.class, () -> new ChaosGame(null, width, height));
    }

    @Test
    @DisplayName("Should throw IllegalArgumentException if width and height are not positive integers.")
    void constructorWithWidthHeightNotPositive() {
      Assertions.assertThrows(IllegalArgumentException.class, () -> new ChaosGame(description, 0, height));
      Assertions.assertThrows(IllegalArgumentException.class, () -> new ChaosGame(description, width, 0));
      Assertions.assertThrows(IllegalArgumentException.class, () -> new ChaosGame(description, -3, height));
      Assertions.assertThrows(IllegalArgumentException.class, () -> new ChaosGame(description, width, -2));
    }

  }
}

