package edu.ntnu.idatt2003.chaos;

import edu.ntnu.idatt2003.model.chaos.ChaosCanvas;
import edu.ntnu.idatt2003.model.exceptions.InvalidNumberException;
import edu.ntnu.idatt2003.model.math.Vector2D;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class ChaosCanvasTest {
  private ChaosCanvas chaosCanvas;
  private Vector2D point;
  private final int width = 100;
  private final int height = 100;
  private final Vector2D minCoords = new Vector2D(0, 0);
  private final Vector2D maxCoords = new Vector2D(10, 10);


  @BeforeEach
  void setUp() {
    point = new Vector2D(5, 5);
    chaosCanvas = new ChaosCanvas(width, height, minCoords, maxCoords);
  }


  @Nested
  @DisplayName("Positive tests for AffineTransform2D.")
  class PositiveTests {

    @Test
    @DisplayName("Positive test for retrieving pixel point.")
    void testGetPixel(){
      int pixel = chaosCanvas.getPixel(point);
      assertEquals(0, pixel);
    }

    @Test
    @DisplayName("Positive test for putting a pixel point.")
    void testPutPixel(){
      // Put pixel on canvas
      chaosCanvas.putPixel(point);
      int pixel = chaosCanvas.getPixel(point);
      assertEquals(1, pixel);
    }

    @Test
    @DisplayName("Positive test for retrieving the width and height of the canvas.")
    void testGetWidthAndHeight(){
      assertEquals(width, chaosCanvas.getWidth());
      assertEquals(height, chaosCanvas.getHeight());
    }
  }


  @Nested
  @DisplayName("Negative Tests ChaosCanvas. Should throw exceptions on invalid input or values.")
  class NegativeTests {
    @Test
    @DisplayName("Constructing ChaosCanvas with null vector should throw NullPointerException.")
    void constructorWithNullVector() {
      assertThrows(NullPointerException.class, () -> new ChaosCanvas(width, height, null, maxCoords));
      assertThrows(NullPointerException.class, () -> new ChaosCanvas(width, height, minCoords, null));
      assertThrows(NullPointerException.class, () -> new ChaosCanvas(width, height, null, null));
    }

    @Test
    @DisplayName("Constructing ChaosCanvas with invalid width and height parameters should throw IllegalArgumentException.")
    void constructorWithInvalidWidthAndHeight() {
      assertThrows(IllegalArgumentException.class, () -> new ChaosCanvas(0, height, minCoords, maxCoords));
      assertThrows(IllegalArgumentException.class, () -> new ChaosCanvas(width, 0, minCoords, maxCoords));
      assertThrows(IllegalArgumentException.class, () -> new ChaosCanvas(-3, height, minCoords, maxCoords));
      assertThrows(IllegalArgumentException.class, () -> new ChaosCanvas(width, -2, minCoords, maxCoords));
    }

    @Test
    @DisplayName("Constructing ChaosGameDescription with maxCoords below or the same as minCoords should throw IllegalArgumentException.")
    void constructorWithMinHigherOrLikeMax() {
      assertThrows(IllegalArgumentException.class, () -> new ChaosCanvas(width, height, minCoords, minCoords));
      assertThrows(IllegalArgumentException.class, () -> new ChaosCanvas(width, height, maxCoords, new Vector2D(3, 10)));
      assertThrows(IllegalArgumentException.class, () -> new ChaosCanvas(width, height, maxCoords, new Vector2D(10, 5)));
    }

    @Test
    @DisplayName("Putting a pixel out of the canvas should throw InvalidNumberException.")
    void putPixelOutOfCanvas() {
      Vector2D pixelOutOfCanvas = new Vector2D(width-1, height+1);
      assertThrows(InvalidNumberException.class, () -> chaosCanvas.putPixel(pixelOutOfCanvas));
    }



  }
}
