package edu.ntnu.idatt2003.chaos;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import edu.ntnu.idatt2003.model.chaos.ChaosGameDescription;
import edu.ntnu.idatt2003.model.math.Complex;
import edu.ntnu.idatt2003.model.math.Vector2D;
import edu.ntnu.idatt2003.model.transformations.JuliaTransform;
import edu.ntnu.idatt2003.model.transformations.Transform2D;
import org.junit.jupiter.api.*;
import java.util.ArrayList;
import java.util.List;

class ChaosGameDescriptionTest {
  private List<Transform2D> transforms;
  private Vector2D minCoords;
  private Vector2D maxCoords;


  @BeforeEach
  void setUp() {
    minCoords = new Vector2D(0, 0);
    maxCoords = new Vector2D(100, 100);

    transforms = new ArrayList<>();
    Complex complex = new Complex(1, 1);
    int sign = 1;
    transforms.add(new JuliaTransform(complex, sign));
  }


  @Nested
  @DisplayName("Positive tests for AffineTransform2D.")
  class PositiveTests {

    @Test
    @DisplayName("Positive test for getter methods after constructing game description.")
    void testConstructorAndGetters(){
      ChaosGameDescription description = new ChaosGameDescription(transforms, minCoords, maxCoords);

      assertEquals(minCoords, description.getMinCoords());
      assertEquals(maxCoords, description.getMaxCoords());
      assertEquals(transforms, description.getTransforms());
    }
  }


  @Nested
  @DisplayName("Negative Tests ChaosGameDescription. Should throw exceptions on invalid input or values.")
  class NegativeTests {
    @Test
    @DisplayName("Constructing ChaosGameDescription with null should throw NullPointerException.")
    void constructorWithNullParameters() {
      assertThrows(NullPointerException.class, () -> new ChaosGameDescription(null, minCoords, maxCoords));
      assertThrows(NullPointerException.class, () -> new ChaosGameDescription(transforms, null, maxCoords));
      assertThrows(NullPointerException.class, () -> new ChaosGameDescription(transforms, minCoords, null));
    }

    @Test
    @DisplayName("Constructing ChaosGameDescription with empty list should throw NullPointerException.")
    void constructorWithEmptyTransformsList() {
      List<Transform2D> emptyTransformsList = new ArrayList<>();
      assertThrows(IllegalArgumentException.class, () -> new ChaosGameDescription(emptyTransformsList, minCoords, maxCoords));
    }
  }
}
