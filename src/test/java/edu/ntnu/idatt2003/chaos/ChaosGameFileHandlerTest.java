package edu.ntnu.idatt2003.chaos;

import edu.ntnu.idatt2003.model.chaos.ChaosGameFileHandler;
import edu.ntnu.idatt2003.model.chaos.ChaosGameDescription;
import edu.ntnu.idatt2003.model.math.Matrix2x2;
import edu.ntnu.idatt2003.model.math.Vector2D;
import edu.ntnu.idatt2003.model.transformations.AffineTransform2D;
import edu.ntnu.idatt2003.model.exceptions.FileProcessingException;
import edu.ntnu.idatt2003.model.exceptions.InvalidTransformationException;
import org.junit.jupiter.api.*;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ChaosGameFileHandlerTest {
  ChaosGameFileHandler chaosGameFileHandler;
  String sierpinskiPath;
  String juliaPath;
  String barnsleyPath;

  @BeforeEach
  void setUp() {
    chaosGameFileHandler = new ChaosGameFileHandler();
    sierpinskiPath = "src/main/resources/transformations/sierpinski_triangle.txt";
    juliaPath = "src/main/resources/transformations/Julia.txt";
    barnsleyPath = "src/main/resources/transformations/barnsley.txt";
  }


  @Nested
  @DisplayName("Positive tests for AffineTransform2D.")
  class PositiveTests {

    @Test
    @DisplayName("readFromFile valid sierpinski path.")
    void readFromFileValidSierpinskiPath() {
      try {
        ChaosGameDescription chaosGameDescription = chaosGameFileHandler.readFromFile(sierpinskiPath);
        assertNotNull(chaosGameDescription);
      } catch (FileProcessingException e) {
        fail("Should not throw FileProcessingException on valid path.");
      }
    }

    @Test
    @DisplayName("readFromFile valid julia path.")
    void readFromFileValidJuliaPath() {
      try {
        ChaosGameDescription chaosGameDescription = chaosGameFileHandler.readFromFile(juliaPath);
        assertNotNull(chaosGameDescription);
      } catch (FileProcessingException e) {
        fail("Should not throw FileProcessingException on valid path.");
      }
    }

    @Test
    @DisplayName("readFromFile valid barnsley path.")
    void readFromFileValidBarnsleyPath() {
      try {
        ChaosGameDescription chaosGameDescription = chaosGameFileHandler.readFromFile(barnsleyPath);
        assertNotNull(chaosGameDescription);
      } catch (FileProcessingException e) {
        fail("Should not throw FileProcessingException on valid path.");
      }
    }
  }


  @Nested
  @DisplayName("Negative Tests for GameFileHandler. Should throw exceptions on invalid input or values.")
  class NegativeTests {
    @Test
    @DisplayName("Fail to read from a file with invalid format")
    void testReadFromInvalidFormatFile() {
      String filePath = "path/to/invalid_format_file.txt";
      assertThrows(FileProcessingException.class, () -> chaosGameFileHandler.readFromFile(filePath));
    }
  }


  @Test
  @DisplayName("Fail to write to an invalid path")
  void testWriteToInvalidPath() {
    ChaosGameDescription description = new ChaosGameDescription(
        List.of(new AffineTransform2D(new Matrix2x2(1, 0, 0, 1), new Vector2D(0, 0))),
        new Vector2D(0, 0), new Vector2D(1, 1));
    assertThrows(FileProcessingException.class, () -> chaosGameFileHandler.writeToFile(description, "/invalid/path/output.txt"));
  }
}

