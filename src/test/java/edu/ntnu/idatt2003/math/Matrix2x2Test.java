package edu.ntnu.idatt2003.math;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import edu.ntnu.idatt2003.model.math.Matrix2x2;
import edu.ntnu.idatt2003.model.math.Vector2D;
import org.junit.jupiter.api.*;

class Matrix2x2Test {
  private Matrix2x2 matrix;
  private Vector2D vector;


  @BeforeEach
  void setUp() {
    matrix = new Matrix2x2(1, 2, 3, 4);
    vector = new Vector2D(1, 2);
  }


  @Nested
  @DisplayName("Positive tests for Matrix2x2.")
  class PositiveTests {

    @Test
    @DisplayName("Positive test for getter methods after constructing matrix.")
    void testConstructor(){
      assertEquals(1, matrix.getA00());
      assertEquals(2, matrix.getA01());
      assertEquals(3, matrix.getA10());
      assertEquals(4, matrix.getA11());
    }

    @Test
    @DisplayName("Positive test method for multiply method. Multiplying matrix with vector.")
    void testMultiplyMatrix() {
      Vector2D result = matrix.multiply(vector);

      assertEquals(5, result.getX0());
      assertEquals(11, result.getX1());
    }

  }

  @Nested
  @DisplayName("Negative Tests Matrix2x2. Should throw exceptions on invalid input.")
  class NegativeTests {
    @Test
    @DisplayName("Multiplying a matrix with null vector should throw a NullPointerException")
    void testMultiplyWithNullVector() {
      assertThrows(NullPointerException.class, () -> matrix.multiply(null));
    }

    @Test
    @DisplayName("Constructing with a null parameter should throw an IllegalArgumentException.")
    void testConstructorWithNull() {
      Assertions.assertThrows(IllegalArgumentException.class,
          () -> new Matrix2x2(Double.NaN, 2, 3, 4));
    }


  }
}
