package edu.ntnu.idatt2003.math;

import edu.ntnu.idatt2003.model.math.Complex;
import edu.ntnu.idatt2003.model.math.Vector2D;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class ComplexTest {

    @Nested
    @DisplayName("Positive tests for Complex.")
    class PositiveTests {
        @Test
        @DisplayName("Positive test for add method in Complex. Should add two complex numbers and return the result")
        void testAddPositiveNumbers() {
            Complex a = new Complex(1.0, 2.0);
            Complex b = new Complex(3.0, 4.0);
            Complex result = a.add(b);
            Assertions.assertEquals(4.0, result.getX0(), "Real part of addition should match");
            Assertions.assertEquals(6.0, result.getX1(), "Imaginary part of addition should match");
        }

        @Test
        @DisplayName("Positive test for add method in Complex. Should add two complex numbers and return the result")
        void testAddNegativeNumbers() {
            Complex a = new Complex(1.0, 2.0);
            Complex b = new Complex(-3.0, -4.0);
            Complex result = a.add(b);
            Assertions.assertEquals(-2.0, result.getX0(), "Real part of addition should match");
            Assertions.assertEquals(-2.0, result.getX1(), "Imaginary part of addition should match");
        }

        @Test
        @DisplayName("Positive test for subtract method in Complex. Should subtract two complex numbers and return the result")
        void testSubtractPositive() {
            Complex a = new Complex(5.0, 4.0);
            Complex b = new Complex(3.0, 2.0);
            Complex result = a.subtract(b);
            Assertions.assertEquals(2.0, result.getX0(), "Real part of subtraction should match");
            Assertions.assertEquals(2.0, result.getX1(), "Imaginary part of subtraction should match");
        }

        @Test
        @DisplayName("Positive test for subtract method in Complex. Should subtract two complex numbers and return the result")
        void testSubtractNegative() {
            Complex a = new Complex(5.0, 4.0);
            Complex b = new Complex(6.0, 5.0);
            Complex result = a.subtract(b);
            Assertions.assertEquals(-1.0, result.getX0(), "Real part of subtraction should match");
            Assertions.assertEquals(-1.0, result.getX1(), "Imaginary part of subtraction should match");
        }
    }

    @Nested
    @DisplayName("Negative Tests Complex. Should throw exceptions on invalid input.")
    class NegativeTests {
        private Complex a;
        private Vector2D notComplex;

        @Test
        @DisplayName("Adding a non-Complex object should throw an IllegalArgumentException")
        void addWithNonComplexShouldThrowException() {
            a = new Complex(1.0, 2.0);
            notComplex = new Vector2D(3.0, 4.0);
            Assertions.assertThrows(IllegalArgumentException.class, () -> a.add(notComplex), "Adding a non-Complex object should throw an IllegalArgumentException");
        }

        @Test
        @DisplayName("Subtracting a non-Complex object should throw an IllegalArgumentException")
        void subtractWithNonComplexShouldThrowException() {
            a = new Complex(1.0, 2.0);
            notComplex = new Vector2D(3.0, 4.0);
            Assertions.assertThrows(IllegalArgumentException.class, () -> a.subtract(notComplex), "Subtracting a non-Complex object should throw an IllegalArgumentException");
        }
    }
}
