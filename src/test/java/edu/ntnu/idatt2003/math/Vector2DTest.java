package edu.ntnu.idatt2003.math;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.idatt2003.model.math.Vector2D;
import org.junit.jupiter.api.*;

class Vector2DTest {
  private Vector2D vector;
  private Vector2D vector2;


  @BeforeEach
  void setUp() {
    vector = new Vector2D(1, 2);
    vector2 = new Vector2D(3, 4);
  }


  @Nested
  @DisplayName("Positive tests for Vector2D.")
  class PositiveTests {

    @Test
    @DisplayName("Positive test for getter methods after constructing vector.")
    void testConstructor(){
      assertEquals(1, vector.getX0());
      assertEquals(2, vector.getX1());
    }

    @Test
    @DisplayName("Positive test method for add method. Add a vector with another vector..")
    void testAddTwoVectors() {
      Vector2D result = vector.add(vector2);

      assertEquals(4, result.getX0());
      assertEquals(6, result.getX1());
    }

  }

  @Nested
  @DisplayName("Negative Tests Vector2D. Should throw exceptions on invalid input.")
  class NegativeTests {
    @Test
    @DisplayName("Adding a null vector should throw a NullPointerException")
    void testAddNullVectorToVector() {
      assertThrows(NullPointerException.class, () -> vector.add(null));
    }

    @Test
    @DisplayName("Subtracting a null vector should throw a NullPointerException")
    void testSubtractNullVectorToVector() {
      assertThrows(NullPointerException.class, () -> vector.subtract(null));
    }

    @Test
    @DisplayName("Constructing with a null parameter should throw an IllegalArgumentException.")
    void testConstructorWithNull() {
      Assertions.assertThrows(IllegalArgumentException.class,
          () -> new Vector2D(Double.NaN, 1));
    }
  }
}
