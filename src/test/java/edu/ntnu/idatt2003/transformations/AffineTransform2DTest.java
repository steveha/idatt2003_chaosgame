package edu.ntnu.idatt2003.transformations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import edu.ntnu.idatt2003.model.math.Matrix2x2;
import edu.ntnu.idatt2003.model.math.Vector2D;
import edu.ntnu.idatt2003.model.transformations.AffineTransform2D;
import org.junit.jupiter.api.*;

class AffineTransform2DTest {
  private Matrix2x2 matrix;
  private Vector2D vector;
  private Vector2D vectorInput;


  @BeforeEach
  void setUp() {
    matrix = new Matrix2x2(1, 2, 3, 4);
    vector = new Vector2D(1, 2);
    vectorInput = new Vector2D(2,3);
  }


  @Nested
  @DisplayName("Positive tests for AffineTransform2D.")
  class PositiveTests {

    @Test
    @DisplayName("Positive test for getter methods after constructing matrix.")
    void testAffineTransform(){
      AffineTransform2D affineTransform = new AffineTransform2D(matrix, vector);
      Vector2D result = affineTransform.transform(vectorInput);

      assertEquals(9, result.getX0());
      assertEquals(20, result.getX1());
    }
  }


  @Nested
  @DisplayName("Negative Tests AffineTransform2D. Should throw exceptions on invalid input.")
  class NegativeTests {
    @Test
    @DisplayName("Creating AffineTransform with null matrix should throw NullPointerException.")
    void testTransformWithNullMatrix() {
      assertThrows(NullPointerException.class, () -> new AffineTransform2D(null, vector));
    }

    @Test
    @DisplayName("Creating AffineTransform with null vector should throw NullPointerException.")
    void testTransformWithNullVector() {
      assertThrows(NullPointerException.class, () -> new AffineTransform2D(matrix, null));
    }

    @Test
    @DisplayName("Constructing with a null parameter should throw an IllegalArgumentException.")
    void testConstructorWithNull(){
      Assertions.assertThrows(NullPointerException.class,
          ()-> new AffineTransform2D(null,null));
    }



  }
}
