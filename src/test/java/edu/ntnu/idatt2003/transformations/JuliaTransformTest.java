package edu.ntnu.idatt2003.transformations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import edu.ntnu.idatt2003.model.math.Complex;
import edu.ntnu.idatt2003.model.math.Vector2D;
import edu.ntnu.idatt2003.model.transformations.JuliaTransform;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class JuliaTransformTest {
    private Vector2D vector;
    private Complex complex;

    @BeforeEach
    void setUp() {
        vector = new Vector2D(1, 1);
        complex = new Complex(0, 0);

    }

    @Nested
    @DisplayName("Positive tests for JuliaTransform.")
    class PositiveTests {
    @Test
    @DisplayName("Positive tests for JuliaTransform.")
    void testJuliaTransform() {
        JuliaTransform transform = new JuliaTransform(complex, 1);

        double expectedX = 1.09868411346781;
        double expectedY = 0.4550898605622274;

        Vector2D result = transform.transform(vector);

        assertEquals(expectedX, result.getX0(), 0.0001);
        assertEquals(expectedY, result.getX1(), 0.0001);
    }
    }

    @Nested
    @DisplayName("Negative tests for JuliaTransform. Should throw exceptions on invalid input.")
    class NegativeTest {
        @Test
        @DisplayName("Transform method with null vector should throw NullPointerException.")
        void testTransformWithNullVector() {
            JuliaTransform transform = new JuliaTransform(complex, 1);
            assertThrows(NullPointerException.class, () -> transform.transform(null));
        }

        @Test
        @DisplayName("Constructor should throw IllegalArgumentException for invalid sign parameter.")
        void constructorWithInvalidSign() {
            assertThrows(IllegalArgumentException.class, () -> new JuliaTransform(complex, 2));
            assertThrows(IllegalArgumentException.class, () -> new JuliaTransform(complex, 0));
            assertThrows(IllegalArgumentException.class, () -> new JuliaTransform(complex, -5));
        }

    }
}


