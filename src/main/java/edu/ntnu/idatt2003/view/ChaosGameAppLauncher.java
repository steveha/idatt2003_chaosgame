package edu.ntnu.idatt2003.view;

import edu.ntnu.idatt2003.view.components.MainView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
/**
 * <p>
 *     Launches the JavaFX application for the Chaos Game. The class initializes
 *     the primary stage and sets the main view as the root node of the scene.
 *     Includes CSS styling for the scene.
 * </p>
 */

public class ChaosGameAppLauncher extends Application {
  /**
   * <p>Starts the primary stage of the Chaos Game application. This method sets up
   * the main view, scene size, and applies CSS styling.</p>
   *
   * <p>It initializes a {@link MainView} instance as the central content of the
   * application, sets the scene size to 1000x700, and specifies the title of the
   * window. Additionally, it uses {@link MainView#activateCssFileStyling} to
   * apply CSS styling to the scene.</p>
   *
   * @param primaryStage The primary stage for this application, onto which
   *        the scene is set and displayed.
   */
  @Override
  public void start(Stage primaryStage) {
    MainView mainView = new MainView();

    // Set main scene
    Scene scene = new Scene(mainView, 1000, 800);
    primaryStage.setTitle("Chaos game");
    primaryStage.setScene(scene);
    primaryStage.show();

    // Activate CSS styling for scene
    mainView.activateCssFileStyling(scene);
  }
  /**
   * <p>The main entry point for all JavaFX applications. The start method is called
   * after the system is ready for the application to begin running.</p>
   *
   * <p>This method launches the JavaFX application environment. The arguments provided
   * to this method are passed on to the application launch.</p>
   *
   * @param args The command line arguments passed to the application. Any arguments
   *        are forwarded to the launch method.
   */

  public static void main(String[] args) {
    launch(args);
  }
}
