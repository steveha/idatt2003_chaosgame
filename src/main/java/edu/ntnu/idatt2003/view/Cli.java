package edu.ntnu.idatt2003.view;

import edu.ntnu.idatt2003.model.chaos.ChaosGame;
import edu.ntnu.idatt2003.model.chaos.ChaosGameDescription;
import edu.ntnu.idatt2003.model.chaos.ChaosGameFileHandler;
import java.util.Scanner;

/**
 * <p>Cli class is a command-line interface for playing the
 * chaos game through a menu-driven interface.
 * The user can read description from a file, write a description to a file,
 * run iterations for a game, print the ASCII fractal, and exit.</p>
 */

public class Cli {
  private final ChaosGameFileHandler chaosGameFileHandler = new ChaosGameFileHandler();
  private ChaosGameDescription chaosGameDescription;
  private ChaosGame chaosGame;
  private final Scanner scanner = new Scanner(System.in);

  /**
   * <p>Method for starting the CLI application by running the main loop.
   * The user is prompted to enter a choice to manage chaos game operations.</p>
   */
  public void start() {
    run();
  }

  /**
   * <p>The main loop for the CLI interface.
   * Displays the menu and handles the user's desired actions based on their input.
   * The loop will continue until the user chooses to exit the application.</p>
   */
  public void run() {
    boolean quitApplication = false;
    while (!quitApplication) {
      printCliMenu();

      try {
        System.out.print("Enter your choice: ");
        int choice = scanner.nextInt();

        switch (choice) {
          case 1 -> readDescriptionFromFile();
          case 2 -> writeDescriptionToFile();
          case 3 -> runIterations();
          case 4 -> writeAsciiFractal();
          case 5 -> quitApplication = true;
          default -> System.out.println("Invalid choice. Choose from 1-5. Please try again.");
        }
      } catch (Exception e) {
        System.out.println("Something went wrong: " + e.getMessage() + "Please try again.");
        scanner.nextLine();
      }
    }
    System.out.println("Exiting...");
    System.exit(0);
  }

  /**
   * <p>Prints out the menu for the chaos game CLI-application.
   * Presents 5 choices the user can choose to perform.
   * The user is prompted to enter a number to choose an option.</p>
   */
  private void printCliMenu() {
    String cliMenu = """
            
            --------MENU FOR CHAOS GAME CLI-APPLICATION-------
            1. Read description from file
            2. Write description to file
            3. Run chosen amount of iterations
            4. Print ASCII-fractal to the terminal
            5. Exit application
            --------------------------------------------------
            
            """;
    System.out.println(cliMenu);
  }

  /**
   * <p>Prints out a list of file names for the user to choose from.
   * Presents 2 choices: barnsley and sierpinski_triangle. </p>
   */
  private void printFileOptions() {
    String fileOptions = """
            
            ---LIST OF FILE NAMES TO CHOOSE FROM---
                          barnsley
                     sierpinski_triangle
            ---------------------------------------
            
            """;
    System.out.println(fileOptions);
  }

  /**
   * <p>Method that reads the chaos game description from a file specified by the user.
   * The user is prompted to enter the name only of the file to read descriptions from.
   * If the file does not exist, a descriptive message will be printed.</p>
   */
  private void readDescriptionFromFile() {
    printFileOptions();
    System.out.println("Please enter the name only of to the file to read a description from: ");
    String fileName = scanner.next().toLowerCase();
    String filePath = "src/main/resources/transformations/" + fileName + ".txt";

    try {
      chaosGameDescription = chaosGameFileHandler.readFromFile(filePath);
      System.out.println("The description was successfully read from the file: " + fileName);

    } catch (Exception e) {
      System.out.println("Something went wrong when trying to read from file: " + e.getMessage());
    }
  }

  /**
   * <p>Method that writes the chaos game description to a file specified by the user.
   * The user is prompted to enter the name only of the file to write descriptions to.
   * If the file does not exist, a descriptive message will be printed.</p>
   */
  private void writeDescriptionToFile() {
    printFileOptions();
    System.out.println("Please enter the name only of to the file to write a description to: ");
    String fileName = scanner.next().toLowerCase();
    String filePath = "src/main/resources/transformations/" + fileName + ".txt";

    try {
      chaosGameFileHandler.writeToFile(chaosGameDescription, filePath);
      System.out.println("The description was successfully written to the file: " + filePath);

    } catch (Exception e) {
      System.out.println("Something went wrong while trying to write to file: " + e.getMessage());
    }
  }

  /**
   * <p>Method that runs iterations on the chaos game.
   * The user is prompted to enter the amount of iterations. If the description does not exist,
   * a descriptive message will be printed, advising the user to first read a description.
   * If the iterations are out of bounds,
   * an exception will be caught and a descriptive message will be printed.</p>
   */
  private void runIterations() {
    if (chaosGameDescription == null) {
      System.out.println("Description was not found.");
      System.out.println("Please choose option 1 to read a description first.");
      return;
    }
    System.out.println("Please enter the number of iterations you want to run: ");
    int iterations = scanner.nextInt();

    try {
      chaosGame = new ChaosGame(chaosGameDescription, 100, 100);
      chaosGame.runSteps(iterations);
      System.out.println("Chaos game simulation completed with " + iterations + "iterations.");

    } catch (ArrayIndexOutOfBoundsException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * <p>Method that prints out the ASCII fractal of the chaos game.
   * If the canvas or chaos game does not exist,
   * a descriptive message will be printed, advising the user
   * to run iterations first.</p>
   */
  private void writeAsciiFractal() {
    try {
      chaosGame.getCanvas().printCanvas();

    } catch (NullPointerException e) {
      System.out.println("There is no game to be printed.");
      System.out.println("Please choose option 3 to run iterations first.");
    }
  }
}