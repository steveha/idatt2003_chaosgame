package edu.ntnu.idatt2003.view.transformviews.affinetransformviews;

import edu.ntnu.idatt2003.view.transformviews.TransformView;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

/**
 * This class provides the field to make a fractal with Affine Transformation.
 * Gaol is to act as a blueprint for when:
 * - Adding new fractal with Affine Transformation
 * - Editing an existing fractal (these input fields will show up, and the user can edit)
 */
public class AffineTransformView extends TransformView {

  /**
   * <p>Constructs an AffineTransformView with a scrollable area for the input fields.
   * It initializes necessary UI components for entering affine transformation data. </p>
   */
  public AffineTransformView() {
    super();

    // ScrollPane if view is too long for the window (since user can add transformation)
    ScrollPane scrollPane = new ScrollPane();
    scrollPane.setContent(contentContainer);
    scrollPane.getStyleClass().add("scroll-pane-values");
    this.getChildren().add(scrollPane);

    contentContainer.getChildren().add(affineInputField());
    applyStyledTextField(minX0, minX1, maxX0, maxX1, steps);

  }

  /**
   * <p>Creates and returns the main
   * input layout for affine transformations.
   * This method sets up text fields for
   * inputting matrix and vector components of an affine transformation. </p>
   *
   * @return A VBox containing the layout for affine transformation input fields.
   */
  public VBox affineInputField() {
    VBox inputLayout = new VBox(30);
    inputLayout.getStyleClass().add("inputField");

    inputLayout.getChildren().addAll(
        createVectorInput("Minimum coordinate", minX0, minX1),
        createVectorInput("Maximum coordinate", maxX0, maxX1),
        createStepsInput(),
        transformationInput());

    return inputLayout;
  }

  /**
   * <p>Constructs and returns a layout for entering
   * a single affine transformation, including a matrix and a vector. </p>
   *
   * @return A VBox containing the UI components for one affine transformation input.
   */
  public VBox transformationInput() {
    TextField a00 = new TextField();
    TextField a01 = new TextField();
    TextField a10 = new TextField();
    TextField a11 = new TextField();
    transformationsValues.add(a00);
    transformationsValues.add(a01);
    transformationsValues.add(a10);
    transformationsValues.add(a11);

    GridPane matrixInput = new GridPane();
    matrixInput.add(createStyledLabel("Matrix:"), 0, 0);
    matrixInput.add(new Label(""), 0, 1);
    matrixInput.add(a00, 0, 1);
    matrixInput.add(a01, 1, 1);
    matrixInput.add(new Label(""), 0, 2);
    matrixInput.add(a10, 0, 2);
    matrixInput.add(a11, 1, 2);

    TextField x0 = new TextField();
    TextField x1 = new TextField();
    transformationsValues.add(x0);
    transformationsValues.add(x1);
    VBox vectorInput = createVectorInput("Vector", x0, x1);

    VBox transformationInput = new VBox(30);
    transformationInput.getChildren().addAll(matrixInput, vectorInput);

    return transformationInput;
  }
}
