package edu.ntnu.idatt2003.view.transformviews.affinetransformviews;

import edu.ntnu.idatt2003.controller.ChaosGameController;
import edu.ntnu.idatt2003.controller.MainViewController;
import edu.ntnu.idatt2003.model.chaos.ChaosGame;
import edu.ntnu.idatt2003.model.exceptions.EmptyFieldException;
import edu.ntnu.idatt2003.model.exceptions.InvalidNumberException;
import edu.ntnu.idatt2003.model.exceptions.InvalidTransformationException;
import edu.ntnu.idatt2003.view.components.AlertView;
import java.util.List;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 * Represents the view when creating a new fractal with Affine Transformation.
 * Extends AffineTransformView, and includes an add-transformation-button and a create-button.
 */
public class AddAffineTransformView extends AffineTransformView {
  private final ChaosGameController chaosGameController;
  private final MainViewController mainViewController;

  /**
   * <p>Constructs an AddAffineTransformView with a reference to the MainViewController.
   * This constructor initializes the view components,
   * and binds actions to buttons for creating fractals,
   * adding transformations, and removing transformations.</p>
   *
   * @param mainViewController A reference to the main view controller
   *                          to communicate with other parts of the application.
   */
  public AddAffineTransformView(MainViewController mainViewController) {
    super();
    this.mainViewController = mainViewController;
    chaosGameController = new ChaosGameController();

    Button createButton = new Button("Create fractal!");
    createButton.getStyleClass().add("lowest-input-container-button");
    createButton.setOnAction(event -> createFractalAction());

    Button addTransButton = getAddTransButton();
    addTransButton.getStyleClass().add("button");

    Button removeTransButton = getRemoveTransButton();
    removeTransButton.getStyleClass().add("button");

    contentContainer.getChildren().addAll(addTransButton, removeTransButton, createButton);
  }

  /**
   * <p>Handles the creation of a Chaos Game based on the current input values.
   * This method validates the inputs, creates the game,
   * and notifies other components of the application
   * about the change, updating the display to show the new fractal. </p>
   */
  private void createFractalAction() {
    try {
      // Validate values in text fields
      validateValues(getMinMaxInput(), getTransformationsValues(), getSteps(), "affine");

      // Get input values and create Chaos Game
      ChaosGame chaosGame = chaosGameController.createNewChaosGameFromInput(
          getMinMaxInput(), getTransformationsValues(), getSteps(), "AFFINE");

      AlertView.showFeedback(
          "Success!",
          "Successfully created a Chaos Game! Click OK to view the fractal.");

      // Notify controller to update the view
      mainViewController.displayEditAffineInputArea(
          getMinMaxInput(), getTransformationsValues(), getSteps());
      mainViewController.displayChaosGameCanvas(chaosGame);

    } catch (EmptyFieldException | InvalidNumberException | InvalidTransformationException e) {
      AlertView.showErrorInvalidInput(e.getMessage());
    }

  }

  /**
   * Dynamically adds a transformation input field to the existing list in the view.
   *
   * @param affineInputField The VBox container where the input fields are displayed.
   */
  private void addTransformationInput(VBox affineInputField) {
    int indexButton = affineInputField.getChildren().size() - 3;
    affineInputField.getChildren().add(indexButton, transformationInput());
  }

  /**
   * Dynamically removes the last transformation input field from the view.
   *
   * @param affineInputField The VBox container from which the input fields are removed.
   */
  private void removeTransformationInput(VBox affineInputField) {
    int numberOfTrans = affineInputField.getChildren().size() - 3;

    if (numberOfTrans > 1) {
      affineInputField.getChildren().remove(numberOfTrans - 1);
      List<TextField> transformationsValues = getTransformationsValues();
      if (transformationsValues.size() >= 6) {
        for (int i = 0; i < 6; i++) {
          transformationsValues.remove(transformationsValues.size() - 1);
        }
      }
    } else {
      AlertView.showError(
          "Invalid action",
          "Must have at least one transformation to create fractal with Affine Transformation.");
    }
  }

  /**
   * Creates a button to add more transformation fields to the form.
   *
   * @return A button configured with an action to add more transformation fields.
   */
  private Button getAddTransButton() {
    Button addTransformationButton = new Button("Add More Transformations");
    addTransformationButton.setOnAction(e -> addTransformationInput(getContentContainer()));
    return addTransformationButton;
  }

  /**
   * Creates a button to remove transformation fields from the form.
   *
   * @return A button configured with an action to remove transformation fields.
   */
  private Button getRemoveTransButton() {
    Button removeTransformationButton = new Button("Remove Transformations");
    removeTransformationButton.setOnAction(e -> removeTransformationInput(getContentContainer()));
    return removeTransformationButton;
  }
}
