package edu.ntnu.idatt2003.view.transformviews.juliatransformviews;

import edu.ntnu.idatt2003.controller.ChaosGameController;
import edu.ntnu.idatt2003.controller.MainViewController;
import edu.ntnu.idatt2003.model.chaos.ChaosGame;
import edu.ntnu.idatt2003.model.exceptions.EmptyFieldException;
import edu.ntnu.idatt2003.model.exceptions.InvalidNumberException;
import edu.ntnu.idatt2003.model.exceptions.InvalidTransformationException;
import edu.ntnu.idatt2003.view.components.AlertView;
import javafx.scene.control.Button;

/**
 * Represents the view when creating a new fractal with Julia Transformation.
 * Extends JuliaTransformView, and includes a create-button.
 */
public class AddJuliaTransformView extends JuliaTransformView {
  private final ChaosGameController chaosGameController;
  private final MainViewController mainViewController;

  /**
   * Constructs an AddJuliaTransformView which
   * provides the user interface for creating Julia fractals.
   * This view includes a button for initiating the fractal creation process
   * based on user input parameters for the Julia set.
   *
   * @param mainViewController The main controller of the application that manages
   *                          transitions between different views and updates the display.
   */
  public AddJuliaTransformView(MainViewController mainViewController) {
    super();
    this.mainViewController = mainViewController;
    chaosGameController = new ChaosGameController();

    Button createButton = new Button("Create fractal!");
    createButton.getStyleClass().add("lowest-input-container-button");
    createButton.setOnAction(event -> createFractalAction());

    contentContainer.getChildren().add(createButton);
  }

  /**
   * <p>Initiates the process of creating a Julia set fractal based on user inputs.
   * This method validates the input fields for creating a fractal,
   * constructs a Chaos Game using those parameters,
   * and then updates the UI to display the newly created fractal. </p>
   */
  private void createFractalAction() {
    try {
      // Validate values in text fields
      validateValues(getMinMaxInput(), getTransformationsValues(), getSteps(), "julia");

      // Get input values and create Chaos Game
      ChaosGame chaosGame = chaosGameController.createNewChaosGameFromInput(
          getMinMaxInput(), getTransformationsValues(), getSteps(), "JULIA");

      AlertView.showFeedback(
          "Success!",
          "Successfully created a Chaos Game! Click OK to view the fractal.");

      // Notify controller to update the view
      mainViewController.displayEditJuliaInputArea(
          getMinMaxInput(), getTransformationsValues(), getSteps());
      mainViewController.displayChaosGameCanvas(chaosGame);

    } catch (EmptyFieldException | InvalidNumberException | InvalidTransformationException e) {
      AlertView.showErrorInvalidInput(e.getMessage());
    }
  }



}
