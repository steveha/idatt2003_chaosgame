package edu.ntnu.idatt2003.view.transformviews;

import edu.ntnu.idatt2003.model.exceptions.EmptyFieldException;
import edu.ntnu.idatt2003.model.exceptions.InvalidNumberException;
import edu.ntnu.idatt2003.model.exceptions.InvalidTransformationException;
import edu.ntnu.idatt2003.util.ValidateInput;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * <p>Represents an abstract base class for views
 * that allow users to input and manage parameters
 * for transformations. This class contains UI elements,
 * and functionality for setting up
 * transformations, including managing minimum,
 * and maximum coordinates, transformation steps,
 * and specific transformation values. </p>
 */
public abstract class TransformView extends VBox {
  protected final TextField minX0;
  protected final TextField minX1;
  protected final TextField maxX0;
  protected final TextField maxX1;
  protected final TextField steps;
  protected final List<TextField> transformationsValues;
  protected final VBox contentContainer;

  /**
   * <p>Constructs a new TransformView by initializing
   * all UI components used to input transformation parameters.
   * This constructor sets up text fields for minimum and,
   * maximum coordinates, the number of transformation steps,
   * and a container for additional transformation-specific values. </p>
   */
  public TransformView() {
    minX0 = new TextField();
    minX1 = new TextField();
    maxX0 = new TextField();
    maxX1 = new TextField();
    steps = new TextField();
    transformationsValues = new ArrayList<>();
    contentContainer = new VBox(30);
    contentContainer.getStyleClass().add("values-container");
    this.getChildren().add(contentContainer);
  }

  /**
   * Applies a style class to a variable number of TextField elements.
   * This method is used to apply a consistent style to text fields
   *
   * @param textFields The TextField elements to which the style class will be applied.
   */
  protected void applyStyledTextField(TextField... textFields) {
    for (TextField textField : textFields) {
      textField.getStyleClass().add("text-field");
    }
  }

  /**
   * Creates and returns a styled Label with the specified text.
   *
   * @param text The text to display in the label.
   * @return A styled Label object.
   */
  protected Label createStyledLabel(String text) {
    Label styledLabel = new Label(text);
    styledLabel.getStyleClass().add("label");
    return styledLabel;
  }

  /**
   * <p>Creates a user interface section for inputting vector coordinates.
   * This method constructs a VBox
   * containing labels and text fields for entering a vector. </p>
   *
   * @param vectorType The description of the vector.
   * @param x0 The TextField for the x-coordinate.
   * @param x1 The TextField for the y-coordinate.
   * @return A VBox containing the vector input UI components.
   */
  protected VBox createVectorInput(String vectorType, TextField x0, TextField x1) {
    VBox vectorSection = new VBox(5);
    vectorSection.getChildren().add(createStyledLabel(vectorType + ":"));

    HBox row = new HBox(10);
    row.getChildren().addAll(createStyledLabel("[x, y] ="), x0, x1);

    vectorSection.getChildren().add(row);
    return vectorSection;
  }

  /**
   * Creates a user interface section for inputting the number of transformation steps.
   *
   * @return A VBox containing the steps input UI components.
   */
  protected VBox createStepsInput() {
    VBox stepsInputSection = new VBox();
    stepsInputSection.getChildren().addAll(createStyledLabel("Amount of steps: "), steps);
    return stepsInputSection;
  }

  /**
   * Validates the input values in the transformation form.
   * This method checks that all provided input fields contain
   * valid, non-empty data suitable for transformation purposes.
   *
   * @param minMaxInput List of TextFields for minimum and maximum coordinates.
   * @param transformationsValues List of TextFields for specific transformation values.
   * @param stepsInput TextField for the number of transformation steps.
   * @param transformType The type of transformation being configured.
   * @throws EmptyFieldException If any required field is empty.
   * @throws InvalidNumberException If any number fields contain invalid numbers.
   * @throws InvalidTransformationException If the transformation type is invalid or unsupported.
   */
  protected void validateValues(
      List<TextField> minMaxInput,
      List<TextField> transformationsValues,
      TextField stepsInput,
      String transformType)
      throws EmptyFieldException, InvalidNumberException, InvalidTransformationException {

    ValidateInput.validateInputs(
        minMaxInput.get(0), minMaxInput.get(1),
        minMaxInput.get(2), minMaxInput.get(3),
        transformationsValues, stepsInput, transformType);
  }

  /**
   * Returns a list of TextFields containing the minimum and,
   * maximum coordinates input by the user.
   *
   * @return A list of TextField objects for min and max coordinates.
   */
  public List<TextField> getMinMaxInput() {
    List<TextField> minMaxInput = new ArrayList<>();
    minMaxInput.add(minX0);
    minMaxInput.add(minX1);
    minMaxInput.add(maxX0);
    minMaxInput.add(maxX1);
    return minMaxInput;
  }

  /**
   * Returns a list of TextFields containing the transformation values input by the user.
   *
   * @return A list of TextField objects for transformation values.
   */
  public List<TextField> getTransformationsValues() {
    return transformationsValues;
  }

  /**
   * Returns the TextField for entering the number of steps in the transformation.
   *
   * @return The TextField for inputting steps.
   */
  public TextField getSteps() {
    return steps;
  }

  /**
   * Returns the main content container of the view.
   *
   * @return The VBox containing all the transformation input components.
   */
  public VBox getContentContainer() {
    return contentContainer;
  }
}
