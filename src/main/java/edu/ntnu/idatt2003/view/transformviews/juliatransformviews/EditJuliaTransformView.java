package edu.ntnu.idatt2003.view.transformviews.juliatransformviews;

import edu.ntnu.idatt2003.view.components.SaveButton;
import java.util.List;
import javafx.scene.control.TextField;


/**
 * Represents the view when creating a new fractal with Affine Transformation.
 */
public class EditJuliaTransformView extends JuliaTransformView {

  /**
   * Constructs a new EditAffineTransformView.
   */
  public EditJuliaTransformView() {
    super();
    SaveButton saveButton = new SaveButton(
        getMinMaxInput(), getTransformationsValues(), getSteps(), "julia");

    getContentContainer().getChildren().add(saveButton);
  }

  /**
   * Method for setting the values for the input fields
   * when this view displays for a certain chaos game fractal.
   *
   * @param minMaxInput List of the minimum and maximum coordinates
   * @param transformationsValues List of the transformation values
   * @param steps the amount of steps
   */
  public void setValues(List<TextField> minMaxInput,
                        List<TextField> transformationsValues,
                        TextField steps) {

    // Sets the minimum coordinate and maximum coordinate
    for (int i = 0; i < minMaxInput.size(); i++) {
      this.getMinMaxInput().get(i).setText(minMaxInput.get(i).getText());
    }

    // Sets the amount of steps
    this.getSteps().setText(steps.getText());

    // Sets the transformation values (real part and imaginary part)
    this.getTransformationsValues().get(0).setText(transformationsValues.get(0).getText());
    this.getTransformationsValues().get(1).setText(transformationsValues.get(1).getText());
  }

}
