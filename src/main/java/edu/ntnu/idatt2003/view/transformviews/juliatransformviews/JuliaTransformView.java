package edu.ntnu.idatt2003.view.transformviews.juliatransformviews;

import edu.ntnu.idatt2003.view.transformviews.TransformView;
import java.util.List;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;


/**
 * This class provides the field to make a fractal with Julia Transformation.
 * Gaol is to act as a blueprint for when:
 * - Adding new fractal with Julia Transformation
 * - Editing an existing fractal (these input fields will show up, and the user can edit)
 */
public class JuliaTransformView extends TransformView {
  private final TextField realPart;
  private final TextField imPart;

  /**
   * Constructs a JuliaTransformView, initializing UI components for complex number input alongside
   * the standard affine transformation fields like minimum and maximum coordinates and steps.
   */
  public JuliaTransformView() {
    super();
    realPart = new TextField();
    imPart = new TextField();
    applyStyledTextField(realPart, imPart);

    contentContainer.getChildren().add(juliaInputField());
    applyStyledTextField(minX0, minX1, maxX0, maxX1, steps, realPart, imPart);
  }

  /**
   * <p>Creates and returns the main input layout for Julia transformations.
   * This layout includes fields for complex numbers,
   * minimum and maximum coordinates, and steps. </p>
   *
   * @return A VBox containing the layout for Julia transformation input fields.
   */
  private VBox juliaInputField() {
    VBox inputLayout = new VBox(30);
    inputLayout.getStyleClass().add("inputField");
    inputLayout.setSpacing(30);

    inputLayout.getChildren().addAll(
        complexNumberInput(),
        createVectorInput("Minimum coordinate", minX0, minX1),
        createVectorInput("Maximum coordinate", maxX0, maxX1),
        createStepsInput());

    return inputLayout;
  }

  /**
   * Constructs and returns a layout for entering a complex number,
   * specifically for the Julia set.
   *
   * @return A VBox containing the UI components for one complex number input.
   */
  private VBox complexNumberInput() {
    VBox complexSection = new VBox(5);
    complexSection.getChildren().add(createStyledLabel("Complex number:"));

    HBox realInput = new HBox(10);
    realInput.getChildren().addAll(createStyledLabel("Real part"), realPart);

    HBox imaginaryInput = new HBox(10);
    imaginaryInput.getChildren().addAll(createStyledLabel("Imaginary part"), imPart);

    complexSection.getChildren().addAll(realInput, imaginaryInput);
    return complexSection;
  }

  /**
   * <p> Returns a list of TextField objects that represent
   * the transformation values specific to the Julia set.
   * This includes both the real and imaginary parts of the complex number.</p>
   *
   * @return A list of TextField objects containing the transformation values for the Julia set.
   */
  public List<TextField> getTransformationsValues() {
    List<TextField> transformationsValues = super.getTransformationsValues();
    transformationsValues.add(realPart);
    transformationsValues.add(imPart);
    return transformationsValues;
  }

}
