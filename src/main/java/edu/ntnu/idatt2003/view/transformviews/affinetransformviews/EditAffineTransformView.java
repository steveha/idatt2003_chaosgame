package edu.ntnu.idatt2003.view.transformviews.affinetransformviews;

import edu.ntnu.idatt2003.view.components.SaveButton;
import java.util.List;
import javafx.scene.control.TextField;

/**
 * <p>Constructs the view for editing existing affine transformations in a chaos game.
 * This view extends AffineTransformView by
 * adding functionality to update and save changes to affine transformations. </p>
 */
public class EditAffineTransformView extends AffineTransformView {

  /**
   * <p>Constructs an EditAffineTransformView which includes
   * a save button configured to handle the saving of edited affine transformations. </p>
   */
  public EditAffineTransformView() {
    super();
    SaveButton saveButton = new SaveButton(
        getMinMaxInput(), getTransformationsValues(), getSteps(), "affine");

    getContentContainer().getChildren().add(saveButton);
  }

  /**
   * <p>Sets the transformation values in the view based
   * on provided data from an existing configuration.
   * This method updates all related fields including minimum,
   * maximum coordinates, steps, and the details of each affine transformation. </p>
   *
   * @param minMaxInput List of TextField elements containing the minimum,
   *                            and maximum coordinate values.
   * @param transformationsValues List of TextField elements containing the
   *                             affine transformation matrix and vector values.
   * @param steps TextField containing the number of steps for the transformation.
   */
  public void setValues(List<TextField> minMaxInput,
                        List<TextField> transformationsValues,
                        TextField steps) {

    // Sets the minimum coordinate and maximum coordinate
    for (int i = 0; i < minMaxInput.size(); i++) {
      this.getMinMaxInput().get(i).setText(minMaxInput.get(i).getText());
    }

    // Set the steps value
    this.getSteps().setText(steps.getText());


    List<TextField> currentTransformationsValues = this.getTransformationsValues();
    int currentSize = currentTransformationsValues.size() / 6;
    int requiredSize = transformationsValues.size() / 6;

    // If we need more transformation input fields, will add.
    if (requiredSize > currentSize) {
      for (int i = currentSize; i < requiredSize; i++) {
        getContentContainer().getChildren().add(
            getContentContainer().getChildren().size() - 1, transformationInput());
      }
    }

    // Set the transformation values
    for (int i = 0; i < requiredSize * 6; i++) {
      currentTransformationsValues.get(i).setText(transformationsValues.get(i).getText());
    }
  }
}