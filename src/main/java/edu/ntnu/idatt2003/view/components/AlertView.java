package edu.ntnu.idatt2003.view.components;

import javafx.scene.control.Alert;

/**
 * <p>The AlertView class provides static utility methods
 * to display alert dialogs in a JavaFX application.
 * It includes methods for showing error and informational
 * alerts with customized titles and messages.
 * The class is as a collection of methods that can be called statically
 * to present different types of alert dialogs.</p>
 */
public class AlertView {

  private AlertView() {
    // Private constructor to prevent instantiation
  }

  /**
   * <p>Configures and displays an Alert dialog in a JavaFX application.
   * This static method sets up an alert with a title, header, and content message,
   * and then displays it, meaning it blocks user interaction with other windows
   * until the alert is acknowledged and closed by the user. </p>
   *
   * @param alert The Alert object to configure and display.
   * @param title The title of the alert dialog, which is also used as the header text.
   * @param message The content text displayed in the body of the alert dialog.
   */
  private static void setUpAlert(Alert alert, String title, String message) {
    alert.setTitle(title);
    alert.setHeaderText(title);
    alert.setContentText(message);

    alert.showAndWait();
  }

  /**
   * <p>Displays an error alert with a specified title and message.
   * The method configures and shows an error dialog that blocks user interaction with
   * other windows of the application until the dialog is dismissed.
   * It is useful for notifying users of errors.</p>
   *
   * @param title   The title of the error dialog window.
   * @param message The detailed message describing the error.
   */
  public static void showError(String title, String message) {
    Alert errorAlert = new Alert(Alert.AlertType.ERROR);

    setUpAlert(errorAlert, title, message);
  }

  /**
   * <p>Displays an informational alert with a specified title and message
   * This method configures and shows an information dialog that blocks user interaction with
   * other windows of the application until the dialog is dismissed.
   * It is useful for providing feedback or additional information to users.</p>
   *
   * @param title   The title of the information dialog window.
   * @param message The message that provides additional information or feedback.
   */
  public static void showFeedback(String title, String message) {
    Alert infoAlert = new Alert(Alert.AlertType.INFORMATION);

    setUpAlert(infoAlert, title, message);
  }

  /**
   * <p>Displays an error alert with a title "INVALID INPUT"
   * This static method is designed to alert users about invalid input errors.
   * It is useful in user interfaces where input validation is critical,
   * and user feedback needs to be clear and immediate.</p>
   *
   * @param message The message that describes the invalid input error.
   */
  public static void showErrorInvalidInput(String message) {
    Alert errorAlert = new Alert(Alert.AlertType.ERROR);

    setUpAlert(errorAlert, "INVALID INPUT", message);
  }
}
