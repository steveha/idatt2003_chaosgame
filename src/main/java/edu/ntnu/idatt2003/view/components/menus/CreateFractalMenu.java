package edu.ntnu.idatt2003.view.components.menus;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

/**
 * <p>The CreateFractalMenu class extends JavaFX's MenuBar,
 * and is specifically designed to provide menu options
 * for creating fractals using different types of transformations.
 * This class includes options for affine and Julia transformations.</p>
 */
public class CreateFractalMenu extends MenuBar {
  private Menu createFractalMenu;
  private MenuItem affineItem;
  private MenuItem juliaItem;

  /**
   * <p>Constructor for the CreateFractalMenu class.
   * This constructor initializes the menu with specific fractal creation options and applies
   * CSS style class to enhance its appearance according to the application's design guidelines.</p>
   */
  public CreateFractalMenu() {
    createFractalMenu = new Menu("Create own fractal");
    createCreateFractalMenu();
    this.getMenus().add(createFractalMenu);

    this.getStyleClass().add("menu-bar-in-main-menu");
  }

  /**
   * Getter for the createFractalMenu property.
   *
   * @return The Menu item for affine transformation.
   */
  public MenuItem getAffineMenuItem() {
    return affineItem;
  }

  /**
   * Getter for the juliaItem property.
   *
   * @return The Menu item for Julia transformation.
   */
  public MenuItem getJuliaMenuItem() {
    return juliaItem;
  }

  /**
   * <p>Initializes and configures the menu items
   * under the "Create own fractal" menu.
   * This method sets up the individual menu items with labels,
   * and adds them to the fractal creation menu.
   * It is called during the construction of the menu bar to organize the menu structure. </p>
   */
  private void createCreateFractalMenu() {
    affineItem = new MenuItem("Affine Transformation");
    juliaItem = new MenuItem("Julia Transformation");

    createFractalMenu.getItems().addAll(affineItem, juliaItem);
  }
}
