package edu.ntnu.idatt2003.view.components;

import edu.ntnu.idatt2003.controller.ChaosGameController;
import edu.ntnu.idatt2003.model.exceptions.EmptyFieldException;
import edu.ntnu.idatt2003.model.exceptions.FileProcessingException;
import edu.ntnu.idatt2003.model.exceptions.InvalidNumberException;
import edu.ntnu.idatt2003.model.exceptions.InvalidTransformationException;
import edu.ntnu.idatt2003.util.ValidateInput;
import java.util.List;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * <p>Represents a specialized button to save the configuration of a Chaos Game.
 * This button integrates directly with ChaosGameController to handle saving operations
 * based on user input, ensuring that all entered data is validated and appropriately processed
 * before attempting to save to a file.</p>
 */
public class SaveButton extends Button {
  private final ChaosGameController chaosGameController;

  /**
   * Constructs a SaveButton object with the specified list of text fields and transformation type.
   *
   * @param minMaxInput List of TextFields containing minimum and maximum coordinates.
   * @param transformationsValues List of TextFields containing transformation matrix values.
   * @param stepsInput TextField containing the number of steps for the chaos game.
   * @param transformType String representing the type of transformation.
   */
  public SaveButton(List<TextField> minMaxInput,
                    List<TextField> transformationsValues,
                    TextField stepsInput,
                    String transformType) {

    super("Save Chaos Game Description");
    this.chaosGameController = new ChaosGameController();
    this.getStyleClass().add("lowest-input-container-button");
    this.setSaveButtonAction(minMaxInput, transformationsValues, stepsInput, transformType);
  }

  /**
   * <p>Configures the action to be performed when the Save button is clicked.
   * This method sets the action on the button that
   * will be executed when the button is clicked.
   * for the chaos game description.</p>
   *
   * @param minMaxInput List of TextFields containing minimum and maximum coordinates.
   * @param transformationsValues List of TextFields containing transformation matrix values.
   * @param stepsInput TextField containing the number of steps for the chaos game.
   * @param transformType String representing the type of transformation.
   */
  public void setSaveButtonAction(
      List<TextField> minMaxInput,
      List<TextField> transformationsValues,
      TextField stepsInput,
      String transformType) {

    this.setOnAction(event -> {
      try {
        saveDescriptionAction(minMaxInput, transformationsValues, stepsInput, transformType);
      } catch (Exception e) {
        AlertView.showErrorInvalidInput(e.getMessage());
      }
    });
  }

  /**
   * <p>Executes the save operation by validating input,
   * and saving the game description through the controller.
   * This method performs input validation,
   * constructs a game description, and attempts to save it to a file.</p>
   *
   * @param minMaxInput List of TextFields containing minimum and maximum coordinates.
   * @param transformationsValues List of TextFields containing transformation matrix values.
   * @param stepsInput TextField containing the number of steps for the chaos game.
   * @param transformType String representing the type of transformation.
   * @throws EmptyFieldException If any field is empty.
   * @throws InvalidNumberException If any numeric input is invalid.
   * @throws NullPointerException If any required input is null.
   * @throws InvalidTransformationException If the transformation type is invalid.
   * @throws FileProcessingException If there is an error saving the file.
   */
  private void saveDescriptionAction(
      List<TextField> minMaxInput,
      List<TextField> transformationsValues,
      TextField stepsInput,
      String transformType)
      throws EmptyFieldException, InvalidNumberException, NullPointerException,
                 InvalidTransformationException, FileProcessingException {

    // Validate values in text fields
    ValidateInput.validateInputs(
        minMaxInput.get(0), minMaxInput.get(1),
        minMaxInput.get(2), minMaxInput.get(3),
        transformationsValues, stepsInput, transformType
    );

    // Gets current stage
    Stage stage = (Stage) this.getScene().getWindow();

    // Delegate save operation to the controller
    boolean isSaved = chaosGameController.isFileSaved(
        minMaxInput, transformationsValues, transformType, stage);

    if (isSaved) {
      AlertView.showFeedback("Success!", "Successfully saved chaos game description to a file");
    } else {
      AlertView.showError("Failed", "File was not saved.");
    }
  }
}
