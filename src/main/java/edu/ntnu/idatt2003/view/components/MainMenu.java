package edu.ntnu.idatt2003.view.components;

import edu.ntnu.idatt2003.view.components.menus.CreateFractalMenu;
import edu.ntnu.idatt2003.view.components.menus.DefinedFractalMenu;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.stage.Stage;


/**
 * <p>Represents the main menu of the graphical user interface for the fractal application.
 * This menu includes options for loading fractals,
 * creating new fractals, and exiting the application. </p>
 */
public class MainMenu extends HBox {
  private DefinedFractalMenu definedFractalMenu;
  private CreateFractalMenu createFractalMenu;
  private LoadFromFileButton loadFromFileButton;

  /**
   * Constructs the main menu and initializes its components.
   */
  public MainMenu() {
    createMenus();
  }

  /**
   * Initializes and arranges the menu components within the horizontal box layout.
   */
  private void createMenus() {
    definedFractalMenu = new DefinedFractalMenu();
    createFractalMenu = new CreateFractalMenu();
    loadFromFileButton = new LoadFromFileButton();

    Region spacer = new Region();
    HBox.setHgrow(spacer, javafx.scene.layout.Priority.ALWAYS);

    this.getChildren().addAll(
        loadFromFileButton, definedFractalMenu, createFractalMenu, spacer, exitButton());
    this.getStyleClass().add("main-menu");
  }

  /**
   * Creates an 'Exit Chaos Game' button that terminates the application.
   *
   * @return A configured 'Exit' button.
   */
  private Button exitButton() {
    Button exitButton = new Button("Exit Chaos Game");
    exitButton.setOnAction(event -> System.exit(0));
    exitButton.getStyleClass().add("button");
    return exitButton;
  }

  /**
   * Opens a file dialog for loading saved content, and displays the loaded data.
   */
  private void openFile() {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Open Saved File");
    FileChooser.ExtensionFilter extFilter = new FileChooser
            .ExtensionFilter("Text files (*.txt)", "*.txt");
    fileChooser.getExtensionFilters().add(extFilter);


    Stage stage = (Stage) this.getScene().getWindow();
    File file = fileChooser.showOpenDialog(stage);

    if (file != null) {
      try {
        String content = new String(Files.readAllBytes(file.toPath()));

        System.out.println("Loaded file: " + content);
      } catch (IOException e) {
        System.err.println("Error reading file: " + e.getMessage());
      }
    }
  }

  /**
   * Gets the menu for predefined fractals.
   *
   * @return The DefinedFractalMenu instance.
   */
  public DefinedFractalMenu getDefinedFractalMenu() {
    return definedFractalMenu;
  }

  /**
   * Gets the menu for creating custom fractals.
   *
   * @return The CreateFractalMenu instance.
   */
  public CreateFractalMenu getCreateFractalMenu() {
    return createFractalMenu;
  }

  /**
   * Gets the button for loading fractals from a file.
   *
   * @return The LoadFromFileButton instance.
   */
  public LoadFromFileButton getLoadFromFileButton() {
    return loadFromFileButton;
  }
}
