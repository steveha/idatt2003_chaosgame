package edu.ntnu.idatt2003.view.components.menus;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

/**
 * <p>The DefinedFractalMenu class extends JavaFX's MenuBar and is designed to provide a menu
 * with predefined fractal options such as the Julia Set, Sierpinski Triangle, and Barnsley Fern.
 * This menu bar allows users to select and interact with these fractals
 * directly through the GUI. </p>
 */
public class DefinedFractalMenu extends MenuBar {
  private Menu definedFractalMenu;
  private MenuItem juliaItem;
  private MenuItem sierpinskiItem;
  private MenuItem barnsleyItem;

  /**
   * <p>Constructor for the DefinedFractalMenu class.
   * Constructor initializes the menu with specific items for predefined fractals and adds it
   * to this menu bar. It also applies a CSS style class to enhance its appearance according to</p>
   */
  public DefinedFractalMenu() {
    definedFractalMenu = new Menu("Pre-defined fractals");
    createDefinedFractalMenu();
    this.getMenus().add(definedFractalMenu);
  }

  /**
   * <p>Initializes and configures the menu items under the "Pre-defined fractals" menu.
   * Method sets up the individual menu items with labels for each predefined fractal
   * and adds them to the defined fractal menu. It assigns a CSS style class to enhance
   * the appearance. </p>
   */
  private void createDefinedFractalMenu() {
    juliaItem = new MenuItem("Julia Set");
    sierpinskiItem = new MenuItem("Sierpinski Triangle");
    barnsleyItem = new MenuItem("Barnsley Fern");
    definedFractalMenu.getItems().addAll(juliaItem, sierpinskiItem, barnsleyItem);

    this.getStyleClass().add("menu-bar-in-main-menu");
  }

  /**
   * Getter for the definedFractalMenu property.
   *
   * @return The Menu item for the defined fractals.
   */
  public Menu getMenu() {
    return definedFractalMenu;
  }

  /**
   * Getter for the juliaItem property.
   *
   * @return The Menu item for the Julia Set fractal.
   */
  public MenuItem getJuliaItem() {
    return juliaItem;
  }

  /**
   * Getter for the sierpinskiItem property.
   *
   * @return The Menu item for the Sierpinski Triangle fractal.
   */
  public MenuItem getSierpinskiItem() {
    return sierpinskiItem;
  }

  /**
   * Getter for the barnsleyItem property.
   *
   * @return The Menu item for the Barnsley Fern fractal.
   */
  public MenuItem getBarnsleyItem() {
    return barnsleyItem;
  }
}