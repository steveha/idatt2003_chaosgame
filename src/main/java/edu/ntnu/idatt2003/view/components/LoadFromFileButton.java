package edu.ntnu.idatt2003.view.components;

import javafx.scene.control.Button;

/**
 * <p> This is a custom button specifically designed to trigger the loading of data from a file.
 * This button is used in the graphical user interfaces where file operations are required,
 * which shows a predefined label "Load from file". </p>
 */
public class LoadFromFileButton extends Button {

  public LoadFromFileButton() {
    super("Load from file");
  }
}
