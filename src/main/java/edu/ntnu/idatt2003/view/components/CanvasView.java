package edu.ntnu.idatt2003.view.components;

import edu.ntnu.idatt2003.model.chaos.ChaosCanvas;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

/**
 * <p>A view for rendering fractal images on a canvas,
 * supporting interactive zoom and pan functionalities.
 * This class provides the necessary user interface elements,
 * and interaction mechanisms to view fractals.
 * It includes handling for mouse events to manipulate the view. </p>
 */
public class CanvasView extends VBox {
  private final Canvas canvas;
  private ChaosCanvas currentChaosCanvas;
  private static final int CANVAS_WIDTH = 800;
  private static final int CANVAS_HEIGHT = 700;

  // Variables for zooming in and zooming out.
  private double scaleFactor = 1.0;
  private double moveX = 0;
  private double moveY = 0;

  // Variables tracking the last position of the mouse
  private double lastX = 0;
  private double lastY = 0;

  /**
   * Constructs a CanvasView and initializes the canvas with predefined dimensions.
   * It sets up mouse event handlers for scrolling, pressing,
   * and dragging to facilitate zooming and panning.
   */
  public CanvasView() {
    this.canvas = new Canvas(CANVAS_WIDTH, CANVAS_HEIGHT);
    this.getChildren().add(canvas);
    // Exists a CSS file for styling, but it doesn't work for some reason

    setMouseEvents();
  }

  /**
   * <p>Configures mouse events for interacting with the canvas.
   * This method sets up handlers for scrolling to zoom,
   * pressing to set drag reference points,
   * and dragging to pan the view. </p>
   */
  private void setMouseEvents() {
    // Declare action for when mouse is scrolled
    canvas.setOnScroll(this::mouseScroll);

    // Declare action for when pressing the mouse
    canvas.setOnMousePressed(this::mousePosition);

    // Declare action for when the mouse gets dragged
    canvas.setOnMouseDragged(this::mouseDragged);
  }

  /**
   * Updates the last known position of the mouse when pressed.
   *
   * @param event The mouse event containing the new coordinates.
   */
  private void mousePosition(MouseEvent event) {
    lastX = event.getX();
    lastY = event.getY();
  }

  /**
   * Handles the mouse dragged event to move
   * the fractal view based on mouse movement.
   *
   * @param event The mouse event containing the current coordinates.
   */
  private void mouseDragged(MouseEvent event) {
    // Calculate the change in the mouse position when pressed
    double changeX = event.getX() - lastX;
    double changeY = event.getY() - lastY;

    // Move the canvas content vertically and horizontally (by the change)
    moveX += changeX;
    moveY += changeY;

    // Update current mouse position
    mousePosition(event);
    drawFractal(currentChaosCanvas);
  }

  /**
   * Handles the mouse scroll event to zoom in or out on the fractal view.
   *
   * @param event The scroll event containing information about how much
   *             and in which direction the scroll wheel was moved.
   */
  private void mouseScroll(ScrollEvent event) {
    // Amount of percent increase/decrease in scale for scrolling
    double zoomFactor = 1.1;

    // Check if user scrolls up (zoom in) or down (zoom out)
    double scale;
    if (event.getDeltaY() > 0) {
      scale = zoomFactor;
    } else {
      scale = 1 / zoomFactor;
    }
    scaleFactor *= scale;

    // Change moveX and moveY so that the mouse keeps its position
    // (zoom in on the mouse's position)
    moveX = event.getX() - scale * (event.getX() - moveX);
    moveY = event.getY() - scale * (event.getY() - moveY);

    drawFractal(currentChaosCanvas);
  }

  /**
   * Renders the fractal from a ChaosCanvas onto the canvas,
   * applying current transformations for scale and translation.
   *
   * @param chaosCanvas The ChaosCanvas object containing the fractal data to be drawn.
   */
  public void drawFractal(ChaosCanvas chaosCanvas) {
    if (chaosCanvas == null) {
      return;
    }

    // Resets the position to standard if the canvas belongs to a new Chaos Game.
    if (currentChaosCanvas != chaosCanvas) {
      resetTransformations();
    }

    // Sets the chaos canvas and determines how the canvas will be scaled and translated.
    currentChaosCanvas = chaosCanvas;
    GraphicsContext gc = canvas.getGraphicsContext2D();
    gc.setTransform(scaleFactor, 0, 0, scaleFactor, moveX, moveY);

    // Clear the canvas with transformed coordinates
    gc.setFill(Color.WHITE);
    gc.fillRect(-moveX / scaleFactor,
        -moveY / scaleFactor,
        canvas.getWidth() / scaleFactor,
        canvas.getHeight() / scaleFactor);

    int[][] canvasMatrix = chaosCanvas.getCanvasArray();
    int rows = canvasMatrix.length;
    int columns = canvasMatrix[0].length;
    double cellWidth = canvas.getWidth() / columns;
    double cellHeight = canvas.getHeight() / rows;

    // Fill out white color where pixel = 1 on gc.
    for (int i = 0; i < rows; i++) {
      for (int j = 0; j < columns; j++) {
        if (canvasMatrix[i][j] == 1) {
          gc.setFill(Color.BLACK);
          gc.fillRect(j * cellWidth, i * cellHeight, cellWidth, cellHeight);
        }
      }
    }
  }

  /**
   * Resets transformations to their default values when a new Chaos Canvas is loaded.
   */
  private void resetTransformations() {
    scaleFactor = 1.0;
    moveX = 0;
    moveY = 0;
  }

  /**
   * Gets the current canvas.
   *
   * @return The Canvas object used for drawing.
   */
  public Canvas getCanvas() {
    return canvas;
  }
}
