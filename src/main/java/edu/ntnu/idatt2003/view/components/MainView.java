package edu.ntnu.idatt2003.view.components;

import edu.ntnu.idatt2003.controller.MainViewController;
import edu.ntnu.idatt2003.model.chaos.ChaosGameFileHandler;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * <p>The main view for the application's user interface,
 * organizing the main menu, input areas, and visualization canvas.
 * This class provides a structured layout that
 * implements the main components of the application. </p>
 */
public class MainView extends VBox {
  private MainMenu menu;
  private VBox inputArea;
  private CanvasView canvas;
  private VBox root;
  private final MainViewController mainViewController;

  /**
   * Constructs the MainView, initializes its components, and sets up the layout.
   */
  public MainView() {
    init();
    VBox root = createLayout();
    getChildren().add(root);

    // Have to have this
    // Sets the MainView as parameter in MainViewController
    mainViewController = new MainViewController(this);
  }

  /**
   * Initializes the key components used within the main view.
   */
  private void init() {
    canvas = new CanvasView();
    menu = new MainMenu();
    inputArea = new VBox();
    root = new VBox();
  }

  /**
   * Creates and returns the primary layout structure for the main view.
   * This method organizes the components into a user-friendly interface.
   *
   * @return A VBox containing the organized layout of the main view components.
   */
  private VBox createLayout() {
    // Create SplitPane and add the inputArea and canvas
    SplitPane splitPane = new SplitPane();
    splitPane.getItems().addAll(inputArea, canvas);
    splitPane.setDividerPositions(0.3); // Adjust the initial divider position as needed

    // Add inputArea and canvasArea to HBox
    HBox inputAndCanvas = new HBox();
    inputAndCanvas.getChildren().addAll(inputArea, canvas);
    inputAndCanvas.getStyleClass().add("input-and-canvas-box");

    // Heading with title: Chaos Game
    TitleHeader titleSection = new TitleHeader();

    root.getChildren().addAll(titleSection, menu, inputAndCanvas);

    return root;
  }

  /**
   * Applies CSS styling to the provided scene using an external stylesheet.
   *
   * @param scene The scene to which the stylesheet will be applied.
   */
  public void activateCssFileStyling(Scene scene) {
    scene.getStylesheets().add(Objects.requireNonNull(
        getClass().getResource("/styles.css")).toExternalForm());
  }

  /**
   * Gets the CanvasView used for fractal rendering.
   *
   * @return The canvas view component.
   */
  public CanvasView getCanvas() {
    return canvas;
  }

  /**
   * Gets the MainMenu component of the interface.
   *
   * @return The main menu component.
   */
  public MainMenu getMainMenu() {
    return menu;
  }

  /**
   * Gets the input area VBox for additional user controls and inputs.
   *
   * @return The input area component.
   */
  public VBox getInputArea() {
    return inputArea;
  }

}
