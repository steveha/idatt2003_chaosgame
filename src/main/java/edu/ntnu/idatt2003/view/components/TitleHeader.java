package edu.ntnu.idatt2003.view.components;

import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * <p> The TitleHeader class extends HBox and is designed
 * to display the title header for the Chaos Game application.
 * This class is responsible for initializing and
 * setting up the title label with appropriate styling. </p>
 */
public class TitleHeader extends HBox {

  /**
   * <p>Constructor for the TitleHeader class.<
   * This constructor initializes the user interface of
   * the title header by invoking the init method,
   * which sets up the label and styles for the header.</p>
   */
  public TitleHeader() {
    init();
  }

  /**
   * <p>Initializes the components and styles of the title header.
   * This method creates a new Label instance for the "Chaos Game" title, adds the appropriate CSS
   * style class, and then adds it to the children of this HBox.
   * It also sets the style class for the HBox itself
   * to visually differentiate the title section from other components.</p>
   */
  private void init() {
    Label title = new Label("Chaos Game");
    title.getStyleClass().add("title");

    this.getChildren().add(title);
    this.getStyleClass().add("title-section");
  }
}