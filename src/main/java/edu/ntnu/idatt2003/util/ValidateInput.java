package edu.ntnu.idatt2003.util;

import edu.ntnu.idatt2003.model.exceptions.EmptyFieldException;
import edu.ntnu.idatt2003.model.exceptions.InvalidNumberException;
import edu.ntnu.idatt2003.model.exceptions.InvalidTransformationException;
import edu.ntnu.idatt2003.model.math.Vector2D;
import java.util.List;
import javafx.scene.control.TextField;

/**
 * <p>Utility class for validating inputs in text fields within
 * a GUI context, especially for transformation-related parameters.
 * This class includes methods to ensure that text inputs
 * are not empty and conform to expected numeric formats,
 * such as integers or doubles, and are within required ranges,
 * or meet specific conditions necessary for processing transformations. </p>
 */
public class ValidateInput {

  private ValidateInput() {

     //Private constructor to prevent instantiation.

  }

  /**
   * Validates that a text field is not empty.
   *
   * @param textField The TextField object to check.
   * @throws EmptyFieldException If the text field is empty.
   */
  public static void validateNotEmptyField(TextField textField) throws EmptyFieldException {
    if (textField.getText().trim().isEmpty()) {
      throw new EmptyFieldException();
    }
  }

  /**
   * Validates that the content of a text field can be successfully parsed as a double.
   *
   * @param textField The TextField object to validate.
   * @param parameterName The name of the parameter to include in any error messages.
   * @throws EmptyFieldException If the text field is empty.
   * @throws InvalidNumberException If the text field content is not a valid double.
   */
  public static void validateDoubleInput(TextField textField, String parameterName)
      throws EmptyFieldException, InvalidNumberException {

    validateNotEmptyField(textField);
    try {
      Double.parseDouble(textField.getText().trim());
    } catch (NumberFormatException e) {
      throw new InvalidNumberException(parameterName, "number");
    }
  }

  /**
   * Validates that the content of a text field can be successfully parsed as an integer.
   *
   * @param textField The TextField object to validate.
   * @param parameterName The name of the parameter to include in any error messages.
   * @throws EmptyFieldException If the text field is empty.
   * @throws InvalidNumberException If the text field content is not a valid integer.
   */
  public static void validateIntInput(TextField textField, String parameterName)
      throws EmptyFieldException, InvalidNumberException {

    validateNotEmptyField(textField);
    try {
      Integer.parseInt(textField.getText().trim());
    } catch (NumberFormatException e) {
      throw new InvalidNumberException(parameterName, "integer");
    }
  }

  /**
   * Validates the minimum and maximum coordinate fields
   * for a transformation, ensuring all coordinates are valid doubles.
   *
   * @param minX0 TextField for minimum x-value.
   * @param minX1 TextField for minimum y-value.
   * @param maxX0 TextField for maximum x-value.
   * @param maxX1 TextField for maximum y-value.
   * @throws EmptyFieldException If any of the fields are empty.
   * @throws InvalidNumberException If any value is not a valid double.
   */
  public static void validateMinMaxCoords(
      TextField minX0, TextField minX1, TextField maxX0, TextField maxX1)
      throws EmptyFieldException, InvalidNumberException {

    validateDoubleInput(minX0, "minimum x-value");
    validateDoubleInput(minX1, "minimum y-value");
    validateDoubleInput(maxX0, "maximum x-value");
    validateDoubleInput(maxX1, "maximum y-value");

    List<TextField> listMinMaxCoord = List.of(minX0, minX1, maxX0, maxX1);

    Vector2D minCoords = TransformInput.getFormattedMinCoords(listMinMaxCoord);
    Vector2D maxCoords = TransformInput.getFormattedMaxCoords(listMinMaxCoord);

    ValidateParameters.validateMinMaxCoords(minCoords, maxCoords);
  }

  /**
   * Validates that a positive integer is entered in a text field.
   *
   * @param textField The TextField object to validate.
   * @param parameterName The name of the parameter to include in any error messages.
   * @throws EmptyFieldException If the text field is empty.
   * @throws InvalidNumberException If the text field content is not a valid positive integer.
   */
  public static void validatePosInt(TextField textField, String parameterName)
      throws EmptyFieldException, InvalidNumberException {

    validateIntInput(textField, parameterName);
    int posInt = Integer.parseInt(textField.getText().trim());
    ValidateParameters.validatePosInt(posInt, parameterName);
  }

  /**
   * Validates the input fields for an affine transformation,
   * matrix and vector components.
   * This method ensures that all necessary inputs for
   * affine transformations are not only present but also valid doubles.
   * Each affine transformation typically requires six inputs:
   * four for the matrix (two rows and two columns) and two for the translation vector.
   *
   * @param transformationsValues A list of TextField objects containing
   *                             the values for multiple affine transformations.
   *                              Each transformation requires six fields.
   * @throws EmptyFieldException If any of the required fields are empty.
   * @throws InvalidNumberException If any of the fields contain values
   *                                  that cannot be parsed as valid doubles.
   */
  private static void validateAffineInputs(List<TextField> transformationsValues)
      throws EmptyFieldException, InvalidNumberException {

    for (int i = 0; i < transformationsValues.size(); i += 6) {
      validateDoubleInput(transformationsValues.get(i), "Row 1 column 1 in matrix");
      validateDoubleInput(transformationsValues.get(i + 1), "Row 1 column 2 in matrix");
      validateDoubleInput(transformationsValues.get(i + 2), "Row 2 column 1 in matrix");
      validateDoubleInput(transformationsValues.get(i + 3), "Row 2 column 2 in matrix");
      validateDoubleInput(transformationsValues.get(i + 4), "x value in vector");
      validateDoubleInput(transformationsValues.get(i + 5), "y value in vector");
    }
  }

  /**
   * Validates the input fields specifically for a Julia transformation,
   * ensuring both real and imaginary parts are valid doubles.
   *
   * @param transformationsValues A list of TextField objects where the first two entries
   *                             represent the real and imaginary parts of a complex number.
   * @throws EmptyFieldException If any of the fields are empty.
   * @throws InvalidNumberException If the text fields do not contain valid doubles.
   */
  private static void validateJuliaInputs(List<TextField> transformationsValues)
      throws EmptyFieldException, InvalidNumberException {
    validateDoubleInput(transformationsValues.get(0), "Real part");
    validateDoubleInput(transformationsValues.get(1), "Imaginary part");
  }

  /**
   * <p>Validates all inputs necessary for configuring a transformation setup,
   * including min/max coordinates, number of transformation steps,
   * and transformation-specific parameters. </p>
   *
   * @param minX0 TextField for the minimum x-coordinate.
   * @param minX1 TextField for the minimum y-coordinate.
   * @param maxX0 TextField for the maximum x-coordinate.
   * @param maxX1 TextField for the maximum y-coordinate.
   * @param transformationsValues List of TextField objects for transformation-specific
   *                             parameters, whose required validations may vary
   *                             by transformation type.
   * @param stepsInput TextField for specifying the number of steps in the transformation.
   * @param transformType A string indicating the type of transformation.
   * @throws EmptyFieldException If any mandatory field is empty.
   * @throws InvalidNumberException If numeric inputs are invalid
   *                                or malformed.
   * @throws InvalidTransformationException If the specified transformation
   *                                type is unsupported or not recognized.
   */
  public static void validateInputs(
      TextField minX0, TextField minX1,
      TextField maxX0, TextField maxX1,
      List<TextField> transformationsValues,
      TextField stepsInput,
      String transformType)
      throws EmptyFieldException, InvalidTransformationException, InvalidNumberException {

    validateMinMaxCoords(minX0, minX1, maxX0, maxX1);
    validatePosInt(stepsInput, "Steps");

    // Validate based on type of transform.
    switch (transformType.toUpperCase()) {
      case "JULIA" -> validateJuliaInputs(transformationsValues);
      case "AFFINE" -> validateAffineInputs(transformationsValues);
      default -> throw new InvalidTransformationException(transformType);
    }
  }
}
