package edu.ntnu.idatt2003.util;

import edu.ntnu.idatt2003.model.chaos.ChaosGameDescription;
import edu.ntnu.idatt2003.model.exceptions.FileProcessingException;
import edu.ntnu.idatt2003.model.exceptions.InvalidNumberException;
import edu.ntnu.idatt2003.model.math.Complex;
import edu.ntnu.idatt2003.model.math.Matrix2x2;
import edu.ntnu.idatt2003.model.math.Vector2D;
import edu.ntnu.idatt2003.model.transformations.Transform2D;
import java.util.List;

/**
 * <p>Utility class providing static methods to
 * validate different types of parameters within the application.
 * This class helps ensure that data passed through
 * the application meets the necessary preconditions for processing,
 * which prevents errors and exceptions during runtime due to invalid or unexpected data. </p>
 */
public class ValidateParameters {

  private ValidateParameters() {
    // Private constructor to prevent instantiation
  }

  /**
   * <p>Validates that the provided double value is not NaN.
   * NaN is a special value that represents
   * "Not a Number" and is used to indicate that a value is not a valid number. </p>
   *
   * @param parameter The double value to validate.
   * @throws InvalidNumberException If the parameter is NaN.
   */
  public static void validateDouble(double parameter) {
    if (Double.isNaN(parameter)) {
      throw new InvalidNumberException();
    }
  }

  /**
   * Ensures that a vector is not null.
   *
   * @param parameter The vector to check.
   * @throws NullPointerException If the vector is null.
   */
  public static void validateVectorNotNull(Vector2D parameter) {
    if (parameter == null) {
      throw new NullPointerException("Vector can not be null.");
    }
  }

  /**
   * Validates parameters of a 2x2 matrix to ensure they are not NaN.
   *
   * @param a00 Top-left element of the matrix.
   * @param a01 Top-right element of the matrix.
   * @param a10 Bottom-left element of the matrix.
   * @param a11 Bottom-right element of the matrix.
   * @throws InvalidNumberException If any parameter is NaN.
   */
  public static void validateMatrix2x2Parameters(double a00, double a01, double a10, double a11)
      throws InvalidNumberException {
    validateDouble(a00);
    validateDouble(a01);
    validateDouble(a10);
    validateDouble(a11);
  }

  /**
   * Ensures that a matrix object is not null.
   *
   * @param matrix The matrix to check.
   * @throws NullPointerException If the matrix is null.
   */
  private static void validateMatrixNotNull(Matrix2x2 matrix) {
    if (matrix == null) {
      throw new NullPointerException("Matrix2x2 can not be null.");
    }
  }

  /**
   * Checks if a vector represents a complex number.
   *
   * @param complex The vector to check.
   * @throws InvalidNumberException If the vector does not represent a complex number.
   */
  public static void validateVectorIsComplex(Vector2D complex) {
    if (!(complex instanceof Complex)) {
      throw new InvalidNumberException("The vector does not represent a complex number.");
    }
  }

  /**
   * Validates that matrix and vector parameters are valid for affine transformations.
   *
   * @param matrix The matrix to validate.
   * @param vector The vector to validate.
   * @throws NullPointerException If either parameter is null.
   */
  public static void validateAffineParameters(Matrix2x2 matrix, Vector2D vector)
      throws NullPointerException {
    validateMatrixNotNull(matrix);
    validateVectorNotNull(vector);
  }

  /**
   * Validates that the sign for Julia transformations is either -1 or 1.
   *
   * @param sign The sign to validate.
   * @throws InvalidNumberException If the sign is not -1 or 1.
   */
  public static void validateJuliaSign(int sign) {
    if (sign != -1 && sign != 1) {
      throw new InvalidNumberException("Sign for Julia Transform can only be -1 or 1.");
    }
  }

  /**
   * Validates that an integer is a positive number.
   *
   * @param parameter The integer to validate.
   * @param parameterName The name of the parameter for error message context.
   * @throws InvalidNumberException If the integer is not positive.
   */
  public static void validatePosInt(int parameter, String parameterName) {
    if (parameter <= 0) {
      throw new InvalidNumberException(parameterName + " must be a positive integer.");
    }
  }

  /**
   * Validates that minimum and maximum coordinates are correct.
   *
   * @param minCoords The minimum coordinates to validate.
   * @param maxCoords The maximum coordinates to validate.
   * @throws InvalidNumberException If min coordinates are not less than max coordinates.
   */
  public static void validateMinMaxCoords(Vector2D minCoords, Vector2D maxCoords) {
    ValidateParameters.validateVectorNotNull(minCoords);
    ValidateParameters.validateVectorNotNull(maxCoords);

    if (minCoords.getX0() >= maxCoords.getX0() || minCoords.getX1() >= maxCoords.getX1()) {
      throw new InvalidNumberException(
          "Maximum must be higher than minimum coordinates.");
    }
  }

  /**
   * Validates that a pixel point is within the bounds of the canvas dimensions.
   *
   * @param x0 The x-coordinate of the point.
   * @param x1 The y-coordinate of the point.
   * @param width The width of the canvas.
   * @param height The height of the canvas.
   * @throws InvalidNumberException If the point is outside the canvas bounds.
   */
  public static void validatePixelPoint(int x0, int x1, int width, int height) {
    if (x0 < 0 || x0 > height || x1 < 0 || x1 > width) {
      throw new InvalidNumberException("The point is outside of the canvas.");
    }
  }

  /**
   * Ensures a list of transformations is neither null nor empty.
   *
   * @param transforms The list of transformations to check.
   * @throws NullPointerException If the list is null.
   * @throws IllegalArgumentException If the list is empty.
   */
  public static void validateTransformsNotEmptyOrNull(List<Transform2D> transforms) {
    if (transforms == null) {
      throw new NullPointerException("The list of transforms can not be null.");
    }
    if (transforms.isEmpty()) {
      throw new IllegalArgumentException("The list of transforms can not be empty.");
    }
  }

  /**
   * Validates that a description for a Chaos Game is not null.
   *
   * @param description The description to validate.
   * @throws NullPointerException If the description is null.
   */
  public static void validateDescNotNull(ChaosGameDescription description) {
    if (description == null) {
      throw new NullPointerException(
          "The description for ChaosGame can not be null.");
    }
  }

  /**
   * Validates that an integer value is not negative.
   *
   * @param notNegInt The integer to validate.
   * @param typeInt A descriptor of what the integer represents.
   * @throws InvalidNumberException If the integer is negative.
   */
  public static void validateNotNegInt(int notNegInt, String typeInt) {
    if (notNegInt < 0) {
      throw new InvalidNumberException("The number of " + typeInt + " must be 0, or higher.");
    }
  }

  /**
   * Validates that the file path ends with a ".txt" extension.
   *
   * @param filePath The file path to validate.
   * @throws FileProcessingException If the file is not a text file.
   */
  public static void validateFileType(String filePath) throws FileProcessingException {
    if (!filePath.endsWith(".txt")) {
      throw new FileProcessingException("The file must be a Text file (.txt)!");
    }
  }
}
