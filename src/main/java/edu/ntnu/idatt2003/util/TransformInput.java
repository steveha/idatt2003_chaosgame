package edu.ntnu.idatt2003.util;

import edu.ntnu.idatt2003.model.exceptions.EmptyFieldException;
import edu.ntnu.idatt2003.model.exceptions.InvalidNumberException;
import edu.ntnu.idatt2003.model.exceptions.InvalidTransformationException;
import edu.ntnu.idatt2003.model.math.Complex;
import edu.ntnu.idatt2003.model.math.Matrix2x2;
import edu.ntnu.idatt2003.model.math.Vector2D;
import edu.ntnu.idatt2003.model.transformations.AffineTransform2D;
import edu.ntnu.idatt2003.model.transformations.JuliaTransform;
import edu.ntnu.idatt2003.model.transformations.Transform2D;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.TextField;

/**
 * <p>Provides static methods to format and validate input from
 * text fields into mathematical transformation components.
 * This class is designed to prevent instantiation,
 * and serves as a utility class for converting user inputs into
 * various mathematical structures like vectors, matrices, and complex numbers.</p>
 */
public class TransformInput {

  private TransformInput() {
    // Private constructor to prevent instantiation
  }

  public static int getFormattedInt(TextField integer) {
    return Integer.parseInt(integer.getText().trim());
  }

  /**
   * Parses and validates two text fields as a 2D vector.
   *
   * @param x0 The TextField for the x-coordinate.
   * @param x1 The TextField for the y-coordinate.
   * @return A new Vector2D object created from the parsed and validated inputs.
   * @throws EmptyFieldException If any input field is empty.
   * @throws InvalidNumberException If any input is not a valid double.
   */
  public static Vector2D getFormattedVector2D(TextField x0, TextField x1)
      throws EmptyFieldException, InvalidNumberException {
    ValidateInput.validateDoubleInput(x0, "x-value in vector");
    ValidateInput.validateDoubleInput(x1, "y-value in vector");
    double x0Coords = Double.parseDouble(x0.getText().trim());
    double x1Coords = Double.parseDouble(x1.getText().trim());
    return new Vector2D(x0Coords, x1Coords);
  }

  /**
   * Parses and validates four text fields into a 2x2 matrix.
   *
   * @param a00 TextField for matrix element a00.
   * @param a01 TextField for matrix element a01.
   * @param a10 TextField for matrix element a10.
   * @param a11 TextField for matrix element a11.
   * @return A new Matrix2x2 object created from the parsed and validated inputs.
   * @throws EmptyFieldException If any input field is empty.
   * @throws InvalidNumberException If any input is not a valid double.
   */
  private static Matrix2x2 getFormattedMatrix2x2(
      TextField a00, TextField a01, TextField a10, TextField a11)
      throws EmptyFieldException, InvalidNumberException {

    ValidateInput.validateDoubleInput(a00, "first row first column");
    ValidateInput.validateDoubleInput(a01, "first row second column");
    ValidateInput.validateDoubleInput(a10, "second row first column");
    ValidateInput.validateDoubleInput(a11, "second row second column");

    double a00Coords = Double.parseDouble(a00.getText().trim());
    double a01Coords = Double.parseDouble(a01.getText().trim());
    double a10Coords = Double.parseDouble(a10.getText().trim());
    double a11Coords = Double.parseDouble(a11.getText().trim());
    return new Matrix2x2(a00Coords, a01Coords, a10Coords, a11Coords);
  }

  /**
   * Retrieves and formats minimum coordinate values from a list of TextFields.
   *
   * @param minMaxInput List containing TextFields for minimum x and y coordinates.
   * @return A Vector2D representing the minimum coordinates.
   * @throws EmptyFieldException If any input field is empty.
   * @throws InvalidNumberException If any input is not a valid double.
   */
  public static Vector2D getFormattedMinCoords(List<TextField> minMaxInput)
      throws EmptyFieldException, InvalidNumberException {
    return getFormattedVector2D(minMaxInput.get(0), minMaxInput.get(1));
  }

  /**
   * Retrieves and formats maximum coordinate values from a list of TextFields.
   *
   * @param minMaxInput List containing TextFields for maximum x and y coordinates.
   * @return A Vector2D representing the maximum coordinates.
   * @throws EmptyFieldException If any input field is empty.
   * @throws InvalidNumberException If any input is not a valid double.
   */
  public static Vector2D getFormattedMaxCoords(List<TextField> minMaxInput)
      throws EmptyFieldException, InvalidNumberException {
    return getFormattedVector2D(minMaxInput.get(2), minMaxInput.get(3));
  }

  /**
   * Parses and validates two text fields as a complex number.
   *
   * @param real TextField for the real part of the complex number.
   * @param imaginary TextField for the imaginary part of the complex number.
   * @return A Complex object representing the parsed complex number.
   * @throws EmptyFieldException If any input field is empty.
   * @throws InvalidNumberException If any input is not a valid double.
   */
  public static Complex getFormattedComplex(TextField real, TextField imaginary)
      throws EmptyFieldException, InvalidNumberException {
    ValidateInput.validateDoubleInput(real, "real part");
    ValidateInput.validateDoubleInput(imaginary, "imaginary part");
    double realPart = Double.parseDouble(real.getText().trim());
    double imagPart = Double.parseDouble(imaginary.getText().trim());
    return new Complex(realPart, imagPart);
  }

  /**
   * Formats a list of TextFields into a list of affine transformations.
   *
   * @param transformationsValues List of TextField objects@
   *                             representing affine transformation parameters.
   * @return A list of AffineTransform2D objects based on the provided TextField values.
   * @throws EmptyFieldException If any input field is empty.
   * @throws InvalidNumberException If any input is not a valid double.
   */
  public static List<Transform2D> formatAffineList(List<TextField> transformationsValues)
      throws EmptyFieldException, InvalidNumberException {
    List<Transform2D> affineTransforms = new ArrayList<>();
    for (int i = 0; i < transformationsValues.size(); i += 6) {
      TextField a00 = transformationsValues.get(i);
      TextField a01 = transformationsValues.get(i + 1);
      TextField a10 = transformationsValues.get(i + 2);
      TextField a11 = transformationsValues.get(i + 3);
      Matrix2x2 matrix = getFormattedMatrix2x2(a00, a01, a10, a11);

      TextField x0 = transformationsValues.get(i + 4);
      TextField x1 = transformationsValues.get(i + 5);
      Vector2D vector = getFormattedVector2D(x0, x1);

      affineTransforms.add(new AffineTransform2D(matrix, vector));
    }
    return affineTransforms;
  }

  /**
   * Formats a list of TextFields into a list of Julia transformations.
   *
   * @param transformationsValues List of TextField objects
   *                             representing Julia transformation parameters.
   * @return A list of JuliaTransform objects based on the provided TextField values.
   * @throws EmptyFieldException If any input field is empty.
   * @throws InvalidNumberException If any input is not a valid double.
   */
  public static List<Transform2D> formatJuliaList(List<TextField> transformationsValues)
      throws EmptyFieldException, InvalidNumberException {
    List<Transform2D> juliaTransforms = new ArrayList<>();
    if (transformationsValues.size() >= 2) {
      TextField realPart = transformationsValues.get(0);
      TextField imagPart = transformationsValues.get(1);
      Complex point = getFormattedComplex(realPart, imagPart);

      // Add both JuliaTransforms with sign -1 and 1
      juliaTransforms.add(new JuliaTransform(point, -1));
      juliaTransforms.add(new JuliaTransform(point, 1));
    }
    return juliaTransforms;
  }

  /**
   * Formats a list of TextField inputs into transformations based on the specified type.
   *
   * @param transformationsValues List of TextField objects representing transformation parameters.
   * @param transformType The type of transformation to create.
   * @return A list of Transform2D objects corresponding to the specified type.
   * @throws EmptyFieldException If any input field is empty.
   * @throws InvalidNumberException If any input is not a valid double.
   * @throws InvalidTransformationException If the transformation type is unsupported.
   */
  public static List<Transform2D> formatTransformations(
      List<TextField> transformationsValues,
      String transformType)
      throws EmptyFieldException, InvalidNumberException, InvalidTransformationException {

    return switch (transformType.toUpperCase()) {
      case "AFFINE" -> formatAffineList(transformationsValues);
      case "JULIA" -> formatJuliaList(transformationsValues);
      default -> throw new InvalidTransformationException(transformType);
    };
  }

}
