package edu.ntnu.idatt2003;

import edu.ntnu.idatt2003.view.ChaosGameAppLauncher;

/**
 * <p>The ChaosGameApp class serves as a simple wrapper to launch the Chaos Game application.
 * This class is designed to initiate the starting point of the application by invoking the
 * main method of the {@link ChaosGameAppLauncher} class. It acts as the entry point for running the
 * application when deployed as an executable or within a development environment.</p>
 */
public class ChaosGameApp {

  /**
   * <p>Main method that serves as the entry point for the application.
   * It delegates the application start to the {@link ChaosGameAppLauncher#main(String[])} method,
   * which initializes and launches the JavaFX application.</p>
   *
   * @param args Command line arguments passed to the application. These are forwarded directly
   *             to the ChaosGameAppLauncher's main method without modification.
   */
  public static void main(String[] args) {
    ChaosGameAppLauncher.main(args);
  }
}