package edu.ntnu.idatt2003.controller;

import edu.ntnu.idatt2003.model.chaos.ChaosGame;
import edu.ntnu.idatt2003.model.exceptions.FileProcessingException;
import edu.ntnu.idatt2003.view.components.AlertView;
import edu.ntnu.idatt2003.view.components.CanvasView;
import edu.ntnu.idatt2003.view.components.LoadFromFileButton;
import edu.ntnu.idatt2003.view.components.MainView;
import edu.ntnu.idatt2003.view.components.menus.CreateFractalMenu;
import edu.ntnu.idatt2003.view.components.menus.DefinedFractalMenu;
import edu.ntnu.idatt2003.view.transformviews.affinetransformviews.AddAffineTransformView;
import edu.ntnu.idatt2003.view.transformviews.affinetransformviews.EditAffineTransformView;
import edu.ntnu.idatt2003.view.transformviews.juliatransformviews.AddJuliaTransformView;
import edu.ntnu.idatt2003.view.transformviews.juliatransformviews.EditJuliaTransformView;
import java.util.List;
import javafx.scene.Node;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * <p>This class provides methods for controlling the main view of the application.
 * It sets up the actions for the menu items and controls the display of the fractals.
 * And it also controls the input area for the user to create new fractals. </p>
 *
 */

public class MainViewController {
  private final MainView mainView;
  private final DefinedFractalMenu definedFractalMenu;
  private final CreateFractalMenu createFractalMenu;
  private final LoadFromFileButton loadFromFileButton;
  private final ChaosGameController chaosGameController;
  private final AddAffineTransformView addAffineTransformView;
  private final AddJuliaTransformView addJuliaTransformView;

  /**
   * <p>Initializes the controller with references to the main view,
   * and its components, and sets up the actions for the menu items.
   * This constructor links the main view with
   * various functional components of the UI such as menus and buttons,
   * and prepares the application's responsiveness to user inputs.</p>
   *
   * @param mainView The MainView instance this controller will manage.
   */
  public MainViewController(MainView mainView) {
    this.mainView = mainView;
    this.definedFractalMenu = mainView.getMainMenu().getDefinedFractalMenu();
    this.createFractalMenu = mainView.getMainMenu().getCreateFractalMenu();
    this.loadFromFileButton = mainView.getMainMenu().getLoadFromFileButton();
    this.chaosGameController = new ChaosGameController();
    this.addAffineTransformView = new AddAffineTransformView(this);
    this.addJuliaTransformView = new AddJuliaTransformView(this);

    setActionPreDefMenuItems();
    setActionCreateFractalMenuItems();
    setLoadFromFileAction();
  }

  /**
   * <p>Assigns a runnable action to a menu item.
   * This method sets the action to be executed
   * when the menu item is selected.</p>
   *
   * @param menuItem The menu item to which the action will be assigned.
   * @param action The action to execute when the menu item is selected.
   */
  private void setActionMenuItem(MenuItem menuItem, Runnable action) {
    menuItem.setOnAction(event -> action.run());
  }

  /**
   * <p>Sets up the predefined fractal menu items with their corresponding actions.
   * These actions involve displaying a
   * specific type of fractal based on the selected menu item. </p>
   */
  private void setActionPreDefMenuItems() {
    setActionMenuItem(definedFractalMenu.getJuliaItem(), () -> displayPreDefSet("JULIA"));
    setActionMenuItem(definedFractalMenu.getSierpinskiItem(), () -> displayPreDefSet("SIERPINSKI"));
    setActionMenuItem(definedFractalMenu.getBarnsleyItem(), () -> displayPreDefSet("BARNSLEY"));
  }

  /**
   * <p>Sets up the create fractal menu items with their corresponding actions.
   * These actions involve rendering specific
   * user interfaces for inputting parameters to create custom fractals. </p>
   */
  private void setActionCreateFractalMenuItems() {
    setActionMenuItem(createFractalMenu.getJuliaMenuItem(),
        () -> displayInputArea(addJuliaTransformView));
    setActionMenuItem(createFractalMenu.getAffineMenuItem(),
        () -> displayInputArea(addAffineTransformView));
  }

  /**
   * <p>Updates and displays the main canvas based on the current fractal data.
   * This involves fetching the latest canvas view from
   * the controller and integrating it into the main application view. </p>
   */
  private void updateCanvasView() {
    // Fetches the canvasView node from chaosGameController
    CanvasView canvasView = chaosGameController.getCanvasView();
    CanvasView canvasContainer = mainView.getCanvas();
    canvasContainer.getChildren().clear();
    canvasContainer.getChildren().add(canvasView.getCanvas());
  }

  /**
   * <p>Displays the specified UI component in the application's input area.
   * This method clears the current state to ensure a fresh display of the given node. </p>
   *
   * @param node The Node to display in the input area.
   */
  private void displayInputArea(Node node) {
    clearState();
    VBox inputArea = mainView.getInputArea();
    inputArea.getChildren().clear();
    inputArea.getChildren().add(node);
  }

  /**
   * <p>Redraws the canvas with the current fractal and updates the visual display.
   * This method calls the ChaosGameController to draw the fractal onto the canvas
   * and then updates the view to reflect the new drawing. </p>
   */
  private void drawAndUpdateCanvas() {
    chaosGameController.drawFractalCanvasView();
    updateCanvasView();
  }

  /**
   * <p>Displays a predefined fractal set based on the specified type.
   * This method resets the application's state,
   * configures the fractal display according to the given type,
   * and then updates the canvas to show the fractal. </p>
   *
   * @param type The type of predefined fractal set to display.
   */
  public void displayPreDefSet(String type) {
    clearState();
    chaosGameController.displayFreDefFractal(type);
    drawAndUpdateCanvas();
  }


  /**
   * <p>Updates the input area to allow editing of affine transformation parameters.
   * It configures an EditAffineTransformView with current parameters and displays it,
   * enabling users to modify and submit new values for affine transformations. </p>
   *
   * @param minMaxInput List of TextFields for minimum and maximum coordinates.
   * @param transformationsValues List of TextFields for transformation matrix and vector values.
   * @param steps TextField for the number of iterative steps.
   */
  public void displayEditAffineInputArea(
      List<TextField> minMaxInput, List<TextField> transformationsValues, TextField steps) {

    EditAffineTransformView editAffineTransformView = new EditAffineTransformView();
    editAffineTransformView.setValues(minMaxInput, transformationsValues, steps);
    displayInputArea(editAffineTransformView);
  }


  /**
   * <p>Updates the input area to allow editing of
   * Julia set transformation parameters.
   * Similar to the affine transformation editor,
   * this sets up an EditJuliaTransformView with current parameters,
   * allowing users to modify and submit new values for Julia transformations. </p>
   *
   * @param minMaxInput List of TextFields for min and max coordinates.
   * @param transformationsValues List of TextFields for Julia transformation parameters.
   * @param steps TextField indicating the number of iterative steps.
   */
  public void displayEditJuliaInputArea(
      List<TextField> minMaxInput, List<TextField> transformationsValues, TextField steps) {

    EditJuliaTransformView editJuliaTransformView = new EditJuliaTransformView();
    editJuliaTransformView.setValues(minMaxInput, transformationsValues, steps);
    displayInputArea(editJuliaTransformView);
  }

  /**
   * <p>Displays the canvas with a specific Chaos Game instance's fractal pattern.
   * This method sets the current Chaos Game in the controller and updates the canvas
   * to display the fractal pattern of the specified Chaos Game. </p>
   *
   * @param chaosGame The Chaos Game instance whose fractal pattern is to be displayed.
   */
  public void displayChaosGameCanvas(ChaosGame chaosGame) {
    this.chaosGameController.setChaosGame(chaosGame);
    drawAndUpdateCanvas();
  }

  /**
   * <p>Clears the current state of the application,
   * preparing the interface for new input or displays.
   * Furthermore the method ensures that previous data
   * or displays do not interfere with new operations,
   * maintaining a clean state for user interactions. </p>
   */
  private void clearState() {
    clearCanvasAndInputField();
    chaosGameController.setChaosGame(null);
  }

  /**
   * <p>Clears both the canvas and input area of the main view.
   * This is part of maintaining a clean application state
   * by removing old data or views from the interface. </p>
   *
   */
  private void clearCanvasAndInputField() {
    Pane canvasContainer = mainView.getCanvas();
    canvasContainer.getChildren().clear();

    VBox inputArea = mainView.getInputArea();
    inputArea.getChildren().clear();
  }

  /**
   * Assigns the action to load a fractal description from a file using the 'Load from file' button.
   */
  private void setLoadFromFileAction() {
    this.loadFromFileButton.setOnAction(event -> loadDescFromFileAction());
  }

  /**
   * Displays and updates the fractal on the canvas.
   */
  private void displayAndUpdateFractal() {
    chaosGameController.drawFractalCanvasView();
    updateCanvasView();
  }

  /**
   * <p>Responsible for loading and displaying fractals from a file.
   * This method is triggered by the 'Load from file' button,
   * and involves opening a file dialog,
   * reading fractal data, and updating the display based on this data. </p>
   */
  private void loadDescFromFileAction() {
    try {
      // Gets current stage
      Stage stage = (Stage) this.loadFromFileButton.getScene().getWindow();

      // display fractal of the file loaded
      if (chaosGameController.canOpenFileAndSetUpGame(stage)) {
        displayAndUpdateFractal();
        AlertView.showFeedback("Success!", "Successfully loaded fractal from file!");
      } else {
        AlertView.showError("Failed!", "No file was chosen");
      }

    } catch (FileProcessingException | IllegalArgumentException | NullPointerException e) {
      AlertView.showError("Failed!", "Failed: " + e.getMessage());
    }
  }
}
