package edu.ntnu.idatt2003.controller;

import edu.ntnu.idatt2003.model.chaos.ChaosGame;
import edu.ntnu.idatt2003.model.chaos.ChaosGameDescription;
import edu.ntnu.idatt2003.model.chaos.ChaosGameDescriptionFactory;
import edu.ntnu.idatt2003.model.chaos.ChaosGameFileHandler;
import edu.ntnu.idatt2003.model.exceptions.EmptyFieldException;
import edu.ntnu.idatt2003.model.exceptions.FileProcessingException;
import edu.ntnu.idatt2003.model.exceptions.InvalidNumberException;
import edu.ntnu.idatt2003.model.exceptions.InvalidTransformationException;
import edu.ntnu.idatt2003.model.math.Vector2D;
import edu.ntnu.idatt2003.model.transformations.Transform2D;
import edu.ntnu.idatt2003.util.TransformInput;
import edu.ntnu.idatt2003.util.ValidateParameters;
import edu.ntnu.idatt2003.view.components.CanvasView;
import java.io.File;
import java.util.List;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * <p>The controller class for the ChaosGame. This class is responsible for setting up the ChaosGame
 * and displaying the fractal on the canvas.
 * Handles the input from the user and creates a new ChaosGame based on the input. </p>
 */
public class ChaosGameController {
  private ChaosGame chaosGame;
  private final CanvasView canvasView;
  private final ChaosGameFileHandler chaosGameFileHandler;
  private static final int STEPS = 1000000;
  private static final int CANVAS_HEIGHT = 800;
  private static final int CANVAS_WIDTH = 1000;


  /**
   * Constructs a ChaosGameController, initializing the associated view components
   * and file handler for saving and loading game descriptions.
   */
  public ChaosGameController() {
    this.canvasView = new CanvasView();
    this.chaosGameFileHandler = new ChaosGameFileHandler();
  }

  /**
   * Sets the current ChaosGame instance.
   *
   * @param chaosGame The ChaosGame instance to be managed by this controller.
   */
  public void setChaosGame(ChaosGame chaosGame) {
    this.chaosGame = chaosGame;
  }

  /**
   * Ensures that a ChaosGame instance is initialized with a given description.
   * If no game exists, it creates a new one. If a game exists, it updates the description.
   *
   * @param description The ChaosGameDescription to initialize or update the game with.
   */
  private void ensureChaosGameInitialized(ChaosGameDescription description) {
    if (this.chaosGame == null) {
      this.chaosGame = new ChaosGame(description, CANVAS_WIDTH, CANVAS_HEIGHT);
    } else {
      this.chaosGame.setNewGameDescription(description);
    }
  }

  /**
   * Returns the CanvasView associated with this controller.
   *
   * @return The CanvasView used for rendering the chaos game.
   */
  public CanvasView getCanvasView() {
    return canvasView;
  }

  /**
   * Renders the current chaos game onto the canvas.
   */
  public void drawFractalCanvasView() {
    this.canvasView.drawFractal(this.chaosGame.getCanvas());
  }

  /**
   * Displays a predefined fractal based on the specified type.
   *
   * @param type The type of fractal to display.
   */
  public void displayFreDefFractal(String type) {
    ChaosGameDescription description = ChaosGameDescriptionFactory.get(type);
    ensureChaosGameInitialized(description);
    this.chaosGame.runSteps(STEPS);
    drawFractalCanvasView();
  }

  /**
   * Converts user input from text fields into transformation objects based on the specified type.
   *
   * @param transformationsValues A list of TextField inputs representing transformation parameters.
   * @param type The type of transformation.
   * @return A list of Transform2D objects created from the input values.
   * @throws InvalidTransformationException If the transformation type is not supported.
   */
  private List<Transform2D> createTransFromInput(
      List<TextField> transformationsValues, String type) {
    return switch (type) {
      case "JULIA" -> TransformInput.formatTransformations(transformationsValues, "JULIA");
      case "AFFINE" -> TransformInput.formatTransformations(transformationsValues, "AFFINE");
      default -> throw new InvalidTransformationException(type);
    };
  }

  /**
   * Creates a new ChaosGame based on user input and starts the game simulation.
   *
   * @param minMaxInput Text fields for minimum and maximum coordinates.
   * @return A new ChaosGame instance configured according to the specified inputs.
   */
  public ChaosGameDescription createDescriptionFromInput(
      List<TextField> minMaxInput,
      List<Transform2D> transformations) {

    Vector2D minCoords = TransformInput.getFormattedMinCoords(minMaxInput);
    Vector2D maxCoords = TransformInput.getFormattedMaxCoords(minMaxInput);
    return new ChaosGameDescription(transformations, minCoords, maxCoords);
  }

  /**
   * Creates a new ChaosGame based on user input and starts the game simulation.
   *
   * @param minMaxInput Text fields for minimum and maximum coordinates.
   * @param transformationsValues Text fields for transformation values.
   * @param stepsInput Text field for the number of steps in the simulation.
   * @param type The type of transformation.
   * @return A new ChaosGame instance configured according to the specified inputs.
   */
  public ChaosGame createNewChaosGameFromInput(
      List<TextField> minMaxInput,
      List<TextField> transformationsValues,
      TextField stepsInput,
      String type) {

    List<Transform2D> transformations =
        createTransFromInput(transformationsValues, type);
    ChaosGameDescription description = createDescriptionFromInput(minMaxInput, transformations);
    int steps = TransformInput.getFormattedInt(stepsInput);

    this.chaosGame = new ChaosGame(description, CANVAS_WIDTH, CANVAS_HEIGHT);
    this.chaosGame.runSteps(steps);

    return chaosGame;
  }

  /**
   * <p>Handles the file saving process, including formatting
   * the input fields into a ChaosGameDescription
   * and using a file chooser dialog to select the save location.</p>
   *
   * @param minMaxInput Text fields for minimum and maximum coordinates.
   * @param transformationsValues Text fields for transformation values.
   * @param transformType The type of transformation.
   * @param stage The primary stage of the application, used for showing the file chooser dialog.
   * @return True if the file was successfully saved, false otherwise.
   * @throws EmptyFieldException If any required field is empty.
   * @throws InvalidNumberException If a number is invalid.
   * @throws NullPointerException If a null value is encountered.
   * @throws InvalidTransformationException If the transformation is invalid.
   * @throws FileProcessingException If an error occurs during file processing.
   */
  public boolean isFileSaved(
      List<TextField> minMaxInput,
      List<TextField> transformationsValues,
      String transformType,
      Stage stage)
      throws EmptyFieldException, InvalidNumberException, NullPointerException,
                 InvalidTransformationException, FileProcessingException {

    // Format input fields to construct a ChaosGameDescription
    List<Transform2D> transformations =
        TransformInput.formatTransformations(transformationsValues, transformType);
    ChaosGameDescription description = createDescriptionFromInput(minMaxInput, transformations);

    // Open file chooser and save to file
    File file = saveFileDialog(stage);

    if (file != null) {
      chaosGameFileHandler.writeToFile(description, file.getAbsolutePath());
      return true;
    }
    return false;
  }

  /**
   * <p>Attempts to open a Chaos Game description file,
   * and set up the game based on its contents.
   * This method opens a file dialog, loads the selected file,
   * and initializes a Chaos Game with the loaded description. </p>
   *
   * @param stage The primary stage of the application, used to anchor the file chooser dialog.
   * @return True if the file was successfully opened and the game was set up, false otherwise.
   * @throws InvalidTransformationException If the transformation type in the file is not supported.
   * @throws FileProcessingException If there are issues reading from the file.
   * @throws NullPointerException If the file path obtained is null.
   */
  public boolean canOpenFileAndSetUpGame(Stage stage)
      throws InvalidTransformationException, FileProcessingException, NullPointerException {
    File selectedFile = loadFileDialog(stage);
    if (selectedFile != null) {
      String filePath = selectedFile.getAbsolutePath();
      ChaosGameDescription description = loadDescFromFile(filePath);
      ensureChaosGameInitialized(description);
      this.chaosGame.runSteps(STEPS);
      return true;
    }
    return false;
  }

  /**
   * <p>Loads a Chaos Game description from a specified file path.
   * This method reads the game configuration,
   * and creates a Chaos Game description object. </p>
   *
   * @param filePath The path of the file to load the description from.
   * @return A ChaosGameDescription object based on the contents of the file.
   * @throws FileProcessingException If there are issues reading the file.
   * @throws InvalidTransformationException If the transformation type
   *                                        specified in the file is invalid.
   * @throws NullPointerException If the provided file path is null.
   */
  public ChaosGameDescription loadDescFromFile(String filePath)
      throws FileProcessingException, InvalidTransformationException, NullPointerException {

    ValidateParameters.validateFileType(filePath);
    return chaosGameFileHandler.readFromFile(filePath);
  }

  /**
   * <p>Opens a save file dialog allowing the user
   * to choose where to save a Chaos Game description.
   * This method configures and displays a save file dialog,
   * filtering for text files. </p>
   *
   * @param stage The stage to which the file dialog is attached.
   * @return The File chosen by the user, or null if no file was selected.
   */
  private File saveFileDialog(Stage stage) {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Save Chaos Game Description to file");
    fileChooser.getExtensionFilters().add(
        new FileChooser.ExtensionFilter("Text file (.txt file)", "*.txt"));
    return fileChooser.showSaveDialog(stage);
  }

  /**
   * <p>Opens a load file dialog allowing the user
   * to select a Chaos Game description file to open.
   * This method configures and displays a load file dialog, filtering for text files. </p>
   *
   * @param stage The stage to which the file dialog is attached.
   * @return The File chosen by the user, or null if no file was selected.
   */
  private File loadFileDialog(Stage stage) {
    FileChooser fileChooser = new FileChooser();
    fileChooser.setTitle("Open Chaos Game Description File");
    fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Text Files", "*.txt"));
    return fileChooser.showOpenDialog(stage);
  }

}
