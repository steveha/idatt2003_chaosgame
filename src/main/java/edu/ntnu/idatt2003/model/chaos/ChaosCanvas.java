package edu.ntnu.idatt2003.model.chaos;

import edu.ntnu.idatt2003.model.exceptions.InvalidNumberException;
import edu.ntnu.idatt2003.model.math.Matrix2x2;
import edu.ntnu.idatt2003.model.math.Vector2D;
import edu.ntnu.idatt2003.model.transformations.AffineTransform2D;
import edu.ntnu.idatt2003.util.ValidateParameters;

/**
 * <p>The ChaosCanvas class manages a graphical canvas for
 * rendering chaos game fractals.
 * It maps coordinate points to canvas pixel indices based on specified bounds
 * and supports basic operations like placing and retrieving pixels,
 * clearing the canvas, and printing the canvas state.</p>
 */
public class ChaosCanvas {
  private final int width;
  private final int height;
  private final int[][] canvas;
  private final Vector2D minCoords;
  private final Vector2D maxCoords;
  private final AffineTransform2D transformCoordsToIndices;

  /**
   * <p>Creates a new ChaosCanvas with the given width, height, and coordinate bounds.
   * The canvas is initialized based on the given width and height,
   * and sets up an affine transformation
   * that maps the specified coordinate bounds
   * to pixel indices within the canvas.</p>
   *
   * @param width the width of the canvas.
   * @param height the height of the canvas
   * @param minCoords the minimum coordinates
   * @param maxCoords the maximum coordinates
   */
  public ChaosCanvas(int width, int height, Vector2D minCoords, Vector2D maxCoords) {
    ValidateParameters.validatePosInt(width, "Width");
    ValidateParameters.validatePosInt(height, "Height");
    ValidateParameters.validateMinMaxCoords(minCoords, maxCoords);
    this.width = width;
    this.height = height;
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
    this.transformCoordsToIndices = new AffineTransform2D(
        new Matrix2x2(
            0.0,
            (height - 1.0) / (minCoords.getX1() - maxCoords.getX1()),
            (width - 1.0) / (maxCoords.getX0() - minCoords.getX0()),
            0.0),
        new Vector2D(
            (((height - 1.0) * maxCoords.getX1()) / (maxCoords.getX1() - minCoords.getX1())),
            ((width - 1.0) * minCoords.getX0()) / (minCoords.getX0() - maxCoords.getX0())));
    this.canvas = new int[height][width];
  }

  /**
   * Retrieves the value of a pixel at the given point.
   *
   * @param point The Vector2D point to transform and retrieve the pixel value.
   * @return The value at the transformed canvas position.
   */
  public int getPixel(Vector2D point) {
    Vector2D indices = transformCoordsToIndices.transform(point);
    int x0 = (int) indices.getX0();
    int x1 = (int) indices.getX1();
    return canvas[x0][x1];
  }

  /**
   * Sets the value of a pixel at a given point after transforming it to canvas indices.
   *
   * @param point The Vector2D point to transform and set the pixel.
   */
  public void putPixel(Vector2D point) throws InvalidNumberException {
    Vector2D indices = transformCoordsToIndices.transform(point);
    int x0 = (int) (indices.getX0());
    int x1 = (int) (indices.getX1());

    ValidateParameters.validatePixelPoint(x0, x1, width, height);

    canvas[x0][x1] = 1;
  }


  /**
   * Returns the canvas as a 2D array.
   *
   * @return the canvas.
   */
  public int[][] getCanvasArray() {
    return canvas;
  }

  /**
   * Clears the canvas by setting all pixels to 0.
   */
  public void clear() {
    for (int i = 0; i < height; i++) {
      for (int j = 0; j < width; j++) {
        canvas[i][j] = 0;
      }
    }
  }

  /**
   * Gets the width of the canvas.
   *
   * @return width of the canvas.
   */
  public int getWidth() {
    return width;
  }


  /**
   * Gets the height of the canvas.
   *
   * @return height of the canvas.
   */
  public int getHeight() {
    return height;
  }


  /**
   * Prints out the current state of the canvas to the console.
   */
  public void printCanvas() {
    for (int[] row : canvas) {
      for (int element : row) {
        if (element == 1) {
          System.out.print("X");
        } else {
          System.out.print(" ");
        }
      }
      System.out.println();
    }
  }

}