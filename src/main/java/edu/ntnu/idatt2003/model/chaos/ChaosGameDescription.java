package edu.ntnu.idatt2003.model.chaos;

import edu.ntnu.idatt2003.model.math.Vector2D;
import edu.ntnu.idatt2003.model.transformations.Transform2D;
import edu.ntnu.idatt2003.util.ValidateParameters;
import java.util.List;

/**
 * <p>The ChaosGameDescription class provides a structured way
 * to define the rules and bounds for a Chaos Game.
 * It contains a list of transformations,
 * and the bounding coordinates that define the area within the game.
 * This class is central to setting up the Chaos Game,
 * allowing the configuration of how points are
 * transformed during the game's progression.</p>
 */
public class ChaosGameDescription {
  private final List<Transform2D> transforms;
  private final Vector2D minCoords;
  private final Vector2D maxCoords;


  /**
   * <p>Creates a new ChaosGameDescription with specified transformations and bounding coordinates.
   * This constructor initializes the ChaosGameDescription with a list of transformations and
   * the minimum and maximum coordinates that define the bounds of the Chaos Game. These parameters
   * are validated to ensure that they meet the game's requirements.</p>
   *
   * @param transforms A list of {@link Transform2D} objects that will
   *                  be used to transform points in the Chaos Game.
   * @param minCoords The lower-left corner coordinates of
   *                 the bounding rectangle for the game.
   * @param maxCoords The upper-right corner coordinates of
   *                 the bounding rectangle for the game.
   * @throws IllegalArgumentException If the transformations list is empty,
   *                null, or if the bounding coordinates are invalid.
   */
  public ChaosGameDescription(
      List<Transform2D> transforms, Vector2D minCoords, Vector2D maxCoords) {

    ValidateParameters.validateMinMaxCoords(minCoords, maxCoords);
    ValidateParameters.validateTransformsNotEmptyOrNull(transforms);

    this.transforms = transforms;
    this.minCoords = minCoords;
    this.maxCoords = maxCoords;
  }


  /**
   * Returns the list of transformations used in the Chaos Game.
   *
   * @return A list of {@link Transform2D} objects representing the transformations.
   */
  public List<Transform2D> getTransforms() {
    return transforms;
  }

  /**
   * Returns the minimum coordinates of the bounding rectangle.
   *
   *  @return The {@link Vector2D} representing the minimum coordinates.
   */
  public Vector2D getMinCoords() {
    return minCoords;
  }

  /**
   * Returns the maximum coordinates of the bounding rectangle for the Chaos Game.
   *
   * @return The {@link Vector2D} representing the maximum coordinates.
   */
  public Vector2D getMaxCoords() {
    return maxCoords;
  }
}