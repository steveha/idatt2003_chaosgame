package edu.ntnu.idatt2003.model.chaos;

import edu.ntnu.idatt2003.model.exceptions.FileProcessingException;
import edu.ntnu.idatt2003.model.exceptions.InvalidTransformationException;
import edu.ntnu.idatt2003.model.math.Complex;
import edu.ntnu.idatt2003.model.math.Matrix2x2;
import edu.ntnu.idatt2003.model.math.Vector2D;
import edu.ntnu.idatt2003.model.transformations.AffineTransform2D;
import edu.ntnu.idatt2003.model.transformations.JuliaTransform;
import edu.ntnu.idatt2003.model.transformations.Transform2D;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>ChaosGameFileHandler class handles reading and,
 * writing of chaos game descriptions to and from files. </p>
 */
public class ChaosGameFileHandler {

  /**
   * <p>Reads a chaos game description from a file and constructs a ChaosGameDescription object.
   * Method reads the contents of a file specified by the filePath,
   * and constructs a ChaosGameDescription object.
   * It ensures that the file content is correctly formatted,
   * and valid for creating a game description.</p>
   *
   * @param filePath The path to the file containing the chaos game description.
   * @return A ChaosGameDescription object representing
   *        the chaos game settings defined in the file.
   * @throws FileProcessingException If there is an error reading
   *        from the file or the file content is not valid.
   */
  public ChaosGameDescription readFromFile(String filePath)
      throws FileProcessingException, InvalidTransformationException, NullPointerException {

    try {
      File file = new File(filePath);
      try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
        return processFile(bufferedReader);
      }
    } catch (IOException e) {
      throw new FileProcessingException("reading from", e.getMessage());
    }
  }

  /**
   * Processes the content of a BufferedReader,
   * to create a chaos game description.
   *
   * @param bufferedReader A BufferedReader wrapping the input
   *                      stream from the chaos game description file.
   * @return A ChaosGameDescription object created from the processed file content.
   * @throws FileProcessingException If there are any issues in processing the file content.
   */
  private ChaosGameDescription processFile(BufferedReader bufferedReader)
      throws FileProcessingException, InvalidTransformationException, NullPointerException {

    try {
      String transformationType = skipComments(bufferedReader.readLine());

      Vector2D minCoords = parseVector(bufferedReader.readLine().trim());
      Vector2D maxCoords = parseVector(bufferedReader.readLine().trim());

      List<Transform2D> transformations = new ArrayList<>();
      String line;

      while ((line = bufferedReader.readLine()) != null) {
        if (!line.trim().isEmpty()) {
          if (transformationType.equals("Julia")) {
            // Add 1 and -1 to julia transform
            transformations.addAll(parseJuliaTransformations(line));
          } else {
            Transform2D transformation = selectTransformation(transformationType, line);
            transformations.add(transformation);
          }
        }
      }
      return new ChaosGameDescription(transformations, minCoords, maxCoords);

    } catch (IOException | NumberFormatException | ArrayIndexOutOfBoundsException e) {
      throw new FileProcessingException("processing", e.getMessage());
    }
  }


  /**
   * Skips comments in a line.
   *
   * @param line the line to skip comments in.
   * @return the line without comments.
   */
  private String skipComments(String line) throws NullPointerException {
    String[] parts = line.split("#");
    return parts[0].trim();
  }

  /**
   * Writes a chaos game description to a file.
   *
   * @param description the chaos game object to write to file.
   * @param path the path to the file to write to.
   * @throws FileProcessingException if the file could not be written.
   */
  public void writeToFile(ChaosGameDescription description, String path)
      throws FileProcessingException, InvalidTransformationException {

    try (BufferedWriter writer = new BufferedWriter(new FileWriter(path))) {
      Transform2D transformFromDesc = description.getTransforms().get(0);

      writeTransformationType(transformFromDesc, writer);
      writeCoordinates(description.getMinCoords(), "Min-coordinate", writer);
      writeCoordinates(description.getMaxCoords(), "Max-coordinate", writer);

      int count = 0;
      for (Transform2D transform : description.getTransforms()) {
        count++;
        writeTransformDetails(transform, count, writer);
        if (transform instanceof JuliaTransform) {
          break;
        }
      }
    } catch (IOException e) {
      throw new FileProcessingException("writing to", e.getMessage());
    }
  }

  /**
   * Writes the type of transformation to the file.
   *
   * @param transform the transformation to write to file.
   * @param writer the writer to use for writing to file.
   * @throws FileProcessingException if the file could not be written.
   */
  private void writeTransformationType(Transform2D transform, BufferedWriter writer)
      throws FileProcessingException, InvalidTransformationException {

    try {
      String typeTransformation = switch (transform.getClass().getSimpleName()) {
        case "AffineTransform2D" -> "Affine2D";
        case "JuliaTransform" -> "Julia";
        default -> throw new InvalidTransformationException(transform.getClass().getSimpleName());
      };
      writer.write(typeTransformation + "    # Type of transformation");
      writer.newLine();

    } catch (IOException e) {
      throw new FileProcessingException("writing transformation type to", e.getMessage());
    }
  }

  /**
   * Writes the coordinates to the file.
   *
   * @param coords the coordinates to write to file.
   * @param label the label to write to file.
   * @param writer the writer to use for writing to file.
   * @throws FileProcessingException  if the file could not be written.
   */
  private void writeCoordinates(Vector2D coords, String label, BufferedWriter writer)
      throws FileProcessingException  {
    try {
      writer.write(coords.getX0() + ", " + coords.getX1() + "    # " + label);
      writer.newLine();

    } catch (IOException e) {
      throw new FileProcessingException("writing coordinates to file", e.getMessage());
    }

  }

  /**
   * Writes the details of the transformation to the file.
   * The details are written based on the type of transformation.
   *
   * @param transform the transformation to write to file.
   * @param count the order number of the transformation.
   * @param writer the writer to use for outputting data.
   * @throws FileProcessingException  if the file could not be written.
   */
  private void writeTransformDetails(Transform2D transform, int count, BufferedWriter writer)
      throws FileProcessingException, InvalidTransformationException {

    try {
      if (transform instanceof AffineTransform2D) {
        writeAffineTransformDetails((AffineTransform2D) transform, count, writer);
      } else if (transform instanceof JuliaTransform) {
        writeJuliaTransformDetails((JuliaTransform) transform, writer);
      } else {
        throw new InvalidTransformationException(transform.getClass().getSimpleName());
      }
      writer.newLine();

    } catch (IOException e) {
      throw new FileProcessingException("writing transformation details to", e.getMessage());
    }
  }

  /**
   * Writes the details of an affine transformation to the file.
   *
   * @param affine the affine transformation to write to file.
   * @param count the order number of the transformation.
   * @param writer the writer to use for outputting data.
   * @throws FileProcessingException if the file could not be written.
   */
  private void writeAffineTransformDetails(
      AffineTransform2D affine, int count, BufferedWriter writer)
      throws FileProcessingException {
    try {
      Matrix2x2 matrix = affine.getMatrix();
      Vector2D vector = affine.getVector();
      String details = String.format("%f, %f, %f, %f, %f, %f    # %d transformation",
          matrix.getA00(), matrix.getA01(), matrix.getA10(), matrix.getA11(),
          vector.getX0(), vector.getX1(), count);
      writer.write(details);

    } catch (IOException e) {
      throw new FileProcessingException("writing affine transformation details to", e.getMessage());
    }
  }

  /**
   * Writes the details of a Julia transformation to the file.
   *
   * @param julia the Julia transformation to write to file.
   * @param writer the writer to use for outputting data.
   * @throws FileProcessingException if the file could not be written.
   */
  private void writeJuliaTransformDetails(JuliaTransform julia, BufferedWriter writer)
      throws FileProcessingException {
    try {
      Complex complexPoint = julia.getPoint();
      String details = String.format("%f, %f    # Real and imaginary part of the complex number",
          complexPoint.getX0(), complexPoint.getX1());
      writer.write(details);

    } catch (IOException e) {
      throw new FileProcessingException("writing julia transformation details to", e.getMessage());
    }
  }

  /**
   * Selects the transformation based on the type of transformation.
   *
   * @param typeOfTransformation the type of transformation to select.
   * @param line the line containing the transformation details.
   * @return A Transform2D object corresponding to the described transformation.
   * @throws InvalidTransformationException if the transformation type is not supported.
   * @throws FileProcessingException if parsing transformation causes an error.
   */
  private Transform2D selectTransformation(String typeOfTransformation, String line)
      throws InvalidTransformationException, FileProcessingException {

    return switch (typeOfTransformation) {
      case "Affine2D" -> parseAffineTransformation(line);
      case "Julia" -> parseJuliaTransformations(line).get(0);
      default -> throw new InvalidTransformationException(typeOfTransformation);
    };
  }


  /**
   * <p>arses a vector from a given line of text, ignoring any comments.
   * This method is used to parse the min and max coordinates of the chaos game. </p>
   *
   * @param line The line of text from which to parse the vector.
   * @return A Vector2D object representing the parsed vector coordinates.
   * @throws FileProcessingException If the line cannot be parsed into a Vector2D object.
   */
  private Vector2D parseVector(String line) throws FileProcessingException, NullPointerException {
    try {
      String numbers = skipComments(line);
      String[] vectorParts = numbers.split(",");

      double x0 = Double.parseDouble(vectorParts[0].trim());
      double x1 = Double.parseDouble(vectorParts[1].trim());

      return new Vector2D(x0, x1);

    } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
      throw new FileProcessingException("parsing vector from", e.getMessage());
    }
  }

  /**
   * Parses a Julia transformation from a line of text,
   * which must include real and imaginary parts of a complex number.
   * This method constructs a JuliaTransform based on the provided complex number components.
   *
   * @param line The line containing the comma-separated
   *            real and imaginary parts of the complex number.
   * @return A JuliaTransform object using the parsed complex number and a default sign.
   * @throws FileProcessingException If the complex number parts cannot be parsed.
   */
  private List<Transform2D> parseJuliaTransformations(String line)
      throws FileProcessingException, NullPointerException {
    try {
      String numbers = skipComments(line);
      String[] parts = numbers.split(",");

      double realPart = Double.parseDouble(parts[0].trim());
      double imaginaryPart = Double.parseDouble(parts[1].trim());
      Complex complexNumber = new Complex(realPart, imaginaryPart);

      // Create list of two Julia transforms (same complex, different sign)
      List<Transform2D> juliaTransforms = new ArrayList<>();
      juliaTransforms.add(new JuliaTransform(complexNumber, -1));
      juliaTransforms.add(new JuliaTransform(complexNumber, 1));

      return juliaTransforms;

    } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
      throw new FileProcessingException("parsing julia transformation from", e.getMessage());
    }
  }


  /**
   * Parses an affine transformation from a given line of text.
   * This method constructs an AffineTransform2D based
   * on the provided matrix elements and vector components.
   *
   * @param line The line containing the comma-separated values for the matrix elements
   *            (a00, a01, a10, a11) and vector coordinates (x0, x1).
   * @return An AffineTransform2D object created from the parsed values.
   * @throws FileProcessingException If the affine transformation parameters cannot be parsed.
   */
  private Transform2D parseAffineTransformation(String line)
      throws FileProcessingException, NullPointerException {
    try {
      String numbers = skipComments(line);
      String[] transformParts = numbers.split(",");

      double a00 = Double.parseDouble(transformParts[0].trim());
      double a01 = Double.parseDouble(transformParts[1].trim());
      double a10 = Double.parseDouble(transformParts[2].trim());
      double a11 = Double.parseDouble(transformParts[3].trim());
      double x0 = Double.parseDouble(transformParts[4].trim());
      double x1 = Double.parseDouble(transformParts[5].trim());

      Matrix2x2 matrix = new Matrix2x2(a00, a01, a10, a11);
      Vector2D vector = new Vector2D(x0, x1);

      return new AffineTransform2D((matrix), vector);

    } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
      throw new FileProcessingException("parsing affine transformation from", e.getMessage());
    }
  }
}