package edu.ntnu.idatt2003.model.chaos;

import edu.ntnu.idatt2003.model.math.Complex;
import edu.ntnu.idatt2003.model.math.Matrix2x2;
import edu.ntnu.idatt2003.model.math.Vector2D;
import edu.ntnu.idatt2003.model.transformations.AffineTransform2D;
import edu.ntnu.idatt2003.model.transformations.JuliaTransform;
import java.util.List;

/**
 * Represents a factory for creating ChaosGameDescription objects.
 * The factory creates different types of chaos games based on the description,
 * such as Julia, Barnsley, and Sierpinski.
 * It is also used to create the chaos game based on the description.
 */

public class ChaosGameDescriptionFactory {


  /**
   * <p>Generates a ChaosGameDescription based on a specified type of fractal game.
   * This method allows for the creation of specific
   * game descriptions by passing a string that defines the type of game.
   * The method returns a ChaosGameDescription object initialized
   * with the correct transformations, and
   * bounding coordinates based on the type.</p>
   *
   * @param descriptionType The type of chaos game for which a description is needed.
   * @return A ChaosGameDescription object configured for the specified type of chaos game.
   * @throws IllegalArgumentException If the provided description type is not recognized.
   */
  public static ChaosGameDescription get(String descriptionType) {
    String type = descriptionType.toUpperCase();
    return switch (type) {
      case "JULIA" -> createJulia();
      case "BARNSLEY" -> createBarnsley();
      case "SIERPINSKI" -> createSierpinski();
      default -> throw new IllegalArgumentException("Unknown type: " + descriptionType);
    };
  }

  /**
   * Creates a Julia chaos game based on the complex number c.
   *
   * @return a ChaosGameDescription object representing the Julia chaos game.
   */
  private static ChaosGameDescription createJulia() {
    Complex c = new Complex(-.74543, .11301);

    return new ChaosGameDescription(
        List.of(
            new JuliaTransform(c, -1),
            new JuliaTransform(c, 1)),
        new Vector2D(-1.6, -1),
        new Vector2D(1.6, 1)
    );
  }

  /**
   * Creates a Sierpinski chaos game.
   *
   * @return a ChaosGameDescription object representing the Sierpinski chaos game.
   */
  private static ChaosGameDescription createSierpinski() {
    return new ChaosGameDescription(
        List.of(
            new AffineTransform2D(new Matrix2x2(0.5, 0.0, 0.0, 0.5), new Vector2D(0.0, 0.0)),
            new AffineTransform2D(new Matrix2x2(0.5, 0.0, 0.0, 0.5), new Vector2D(0.25, 0.50)),
            new AffineTransform2D(new Matrix2x2(0.5, 0.0, 0.0, 0.5), new Vector2D(0.5, 0.0))
        ),
        new Vector2D(0.0, 0.0),
        new Vector2D(1.0, 1.0)
    );
  }

  /**
   * Creates a Barnsley chaos game.
   *
   * @return a ChaosGameDescription object representing the Barnsley chaos game.
   */
  private static ChaosGameDescription createBarnsley() {
    return new ChaosGameDescription(
        List.of(
            new AffineTransform2D(new Matrix2x2(0.0, 0.0, 0.0, 0.16), new Vector2D(0.0, 0.0)),
            new AffineTransform2D(new Matrix2x2(0.85, 0.04, -0.04, 0.85), new Vector2D(0.0, 1.60)),
            new AffineTransform2D(new Matrix2x2(0.20, -0.26, 0.23, 0.22), new Vector2D(0.0, 1.60)),
            new AffineTransform2D(new Matrix2x2(-0.15, 0.28, 0.26, 0.24), new Vector2D(0.0, 0.44))
        ),
        new Vector2D(-2.65, 0.0),
        new Vector2D(2.65, 10.0)
    );
  }
}