package edu.ntnu.idatt2003.model.chaos;

/**
 * <p>The ChaosGameObserver interface defines the method that
 * must be implemented by any object that wishes to be
 * notified of changes in the state of a ChaosGame.
 * This interface is of an Observer design pattern,
 * which is used to create a subscription mechanism
 * for objects that need to be notified of changes in
 * the state of another object.</p>
 */
public interface ChaosGameObserver {
  void update();
}

