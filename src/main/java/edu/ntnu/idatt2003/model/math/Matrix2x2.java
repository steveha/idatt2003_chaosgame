package edu.ntnu.idatt2003.model.math;

import edu.ntnu.idatt2003.util.ValidateParameters;

/**
 * <p>The Matrix2x2 class represents a 2x2 matrix.
 * Includes a constructor to initialize the matrix,
 * methods to retrieve the matrix elements, and a method to perform
 * matrix-vector multiplication.</p>
 */

public class Matrix2x2 {
  private double a00;
  private double a01;
  private double a10;
  private double a11;

  /**
   * Creates a Matrix2x2 with specified elements.
   *
   * @param a00 Element at the first row, first column.
   * @param a01 Element at the first row, second column.
   * @param a10 Element at the second row, first column.
   * @param a11 Element at the second row, second column.
   * @throws IllegalArgumentException if any of the parameters do not satisfy validation criteria.
   */
  public Matrix2x2(double a00, double a01, double a10, double a11) {
    ValidateParameters.validateMatrix2x2Parameters(a00, a01, a10, a11);
    this.a00 = a00;
    this.a01 = a01;
    this.a10 = a10;
    this.a11 = a11;
  }

  /**
   * Returns the element at the first row, first column of the matrix.
   *
   * @return Value of a00.
   */
  public double getA00() {
    return a00;
  }

  /**
   * Returns the element at the first row, second column.
   *
   * @return Value of a01.
   */
  public double getA01() {
    return a01;
  }

  /**
   * Returns the element at the second row, first column.
   *
   * @return Value of a10.
   */
  public double getA10() {
    return a10;
  }

  /**
   * Returns the element at the second row, second column.
   *
   * @return Value of a11.
   */
  public double getA11() {
    return a11;
  }

  /**
   * <p>Multiplies this matrix by the given vector.
   * It performs matrix-vector multiplication using
   * the elements of this matrix and a given vector.</p>
   *
   * @param other The vector to multiply with this matrix.
   * @return The resulting {@link Vector2D} after multiplication.
   * @throws NullPointerException if the other vector is null.
   */
  public Vector2D multiply(Vector2D other) {
    ValidateParameters.validateVectorNotNull(other);
    return new Vector2D(
            a00 * other.getX0() + a01 * other.getX1(),
            a10 * other.getX0() + a11 * other.getX1()
    );
  }
}