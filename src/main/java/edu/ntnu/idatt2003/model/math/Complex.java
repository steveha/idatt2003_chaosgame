package edu.ntnu.idatt2003.model.math;

import edu.ntnu.idatt2003.util.ValidateParameters;

/**
 * <p>The Complex class extends the {@link Vector2D} class to represent complex numbers
 * in a two-dimensional space where the x-coordinate corresponds to the real part and
 * the y-coordinate corresponds to the imaginary part of the complex number.</p>
 * Class provides methods to perform arithmetic operations on complex numbers
 * including addition, subtraction, and taking the square root. </p>
 */
public class Complex extends Vector2D {

  /**
   * Creates a new complex number with the given real and imaginary parts.
   *
   * @param realPart the real part
   * @param imaginaryPart the imaginary part
   */
  public Complex(double realPart, double imaginaryPart) {
    super(realPart, imaginaryPart);
  }

  /**
   * Calculates and returns the square root of this complex number.
   *
   * @return New complex number representing the square root of this complex number
   */
  public Complex sqrt() {
    double realPart = this.getX0();
    double imaginaryPart = this.getX1();

    double r = Math.sqrt(Math.pow(realPart, 2) + Math.pow(imaginaryPart, 2));

    double sqrtRealPart = Math.sqrt((r + realPart) / 2);
    double sqrtImaginaryPart = Math.signum(imaginaryPart) * Math.sqrt((r - realPart) / 2);

    return new Complex(sqrtRealPart, sqrtImaginaryPart);
  }

  /**
   * <p>Adds another complex number to this complex number and returns the result.
   * This ensures the other vector is a Complex instance before performing the addition,
   * preserving the integrity of complex number operations.</p>
   *
   * @param other The other Vector2D (must be a Complex instance) to add to this complex number.
   * @return A new Complex object representing the sum of this and the other complex number.
   * @throws IllegalArgumentException if the other vector is not a complex number.
   */
  @Override
  public Complex add(Vector2D other) {
    ValidateParameters.validateVectorIsComplex(other);
    return new Complex(this.getX0() + other.getX0(), this.getX1() + other.getX1());
  }

  /**
   * <p>Subtracts another complex number from this complex number and returns the result.
   * This method ensures the other vector is a Complex instance before performing the subtraction,
   * maintaining correct behavior for complex number operations.</p>
   *
   * @param other The other Vector2D  to subtract from this complex number.
   * @return A new Complex object representing the difference
   *        between this and the other complex number.
   * @throws IllegalArgumentException if the other vector is not a complex number.
   */
  @Override
  public Complex subtract(Vector2D other) {
    ValidateParameters.validateVectorIsComplex(other);
    return new Complex(this.getX0() - other.getX0(), this.getX1() - other.getX1());
  }
}