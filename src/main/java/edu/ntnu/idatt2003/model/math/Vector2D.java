package edu.ntnu.idatt2003.model.math;

import edu.ntnu.idatt2003.util.ValidateParameters;
import java.util.Objects;

/**
 * The Vector2D class represents a vector in a two-dimensional space with x and y components.
 */
public class Vector2D {
  private double x0;
  private double x1;

  /**
   * Constructs a new Vector2D object with specified x (x0) and y (x1) components.
   * <p>Each component is validated to ensure it is
   * a valid double value using {@link ValidateParameters#validateDouble}.</p>
   *
   * @param x0 The x-component of the vector.
   * @param x1 The y-component of the vector.
   * @throws IllegalArgumentException if any of the coordinates are invalid.
   */
  public Vector2D(double x0, double x1) {
    ValidateParameters.validateDouble(x0);
    ValidateParameters.validateDouble(x1);
    this.x0 = x0;
    this.x1 = x1;
  }

  /**
   * Returns the x-component of the vector.
   *
   * @return The x-coordinate x0.
   */
  public double getX0() {
    return x0;
  }

  /**
   * Returns the y-component of the vector.
   *
   * @return The y-coordinate x1.
   */
  public double getX1() {
    return x1;
  }

  /**
   * <p>Adds another vector to this vector and returns the resulting vector.
   * The other vector must not be null,
   * and this condition is validated before performing the operation.</p>
   *
   * @param other The other Vector2D to add to this vector.
   * @return A new Vector2D that is the result of adding the other vector to this one.
   * @throws NullPointerException if the other vector is null.
   */
  public Vector2D add(Vector2D other) {
    ValidateParameters.validateVectorNotNull(other);
    return new Vector2D(this.x0 + other.x0, this.x1 + other.x1);
  }

  /**
   * Subtracts another vector from this vector and returns the resulting vector.
   * The other vector must not be null,
   * and this condition is validated before performing the operation.</p>
   *
   * @param other The other Vector2D to subtract from this vector.
   * @return A new Vector2D that is the result of subtracting the other vector from this one.
   * @throws NullPointerException if the other vector is null.
   */
  public Vector2D subtract(Vector2D other) {
    ValidateParameters.validateVectorNotNull(other);
    return new Vector2D(this.x0 - other.x0, this.x1 - other.x1);
  }

  /**
   * Compares this vector to the specified object for equality.
   *
   * @param o The object to compare this {@code Vector2D} against.
   * @return {@code true} if the given object represents a
   *          {@code Vector2D} equivalent to this vector, {@code false} otherwise.
   */

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Vector2D vector2D = (Vector2D) o;
    return Double.compare(vector2D.x0, x0) == 0 && Double.compare(vector2D.x1, x1) == 0;
  }

  /**
   * Returns a hash code value for this vector.
   *
   * @return A hash code value for this vector.
   */
  @Override
  public int hashCode() {
    return Objects.hash(x0, x1);
  }
}