package edu.ntnu.idatt2003.model.exceptions;

import java.io.IOException;

/**
 * FileProcessingException
 * This class is a custom exception that extends {@link java.io.IOException}.
 * It is used to handle exceptions that occur during file processing operations.
 */
public class FileProcessingException extends IOException {

  /**
   * <p> Constructs a new FileProcessingException with a standard message indicating
   * that an unspecified error occurred during file processing.</p>
   */
  public FileProcessingException() {
    super("An error occurred while processing a file");
  }

  /**
   * <p>Constructs a new FileProcessingException with the specified detail message.
   * It allows for specifying a detailed message that describes the
   * specific error that occurred during file processing.</p>
   *
   * @param message The detail message that describes the error that occurred.
   */
  public FileProcessingException(String message) {
    super(message);
  }

  /**
   * <p>This constructor allows for specifying the action during which the error occurred,
   * and includes the description of the exception or error.
   * It constructs a detailed message that combines
   * both the action and the error description.</p>
   *
   * @param action The action that was being performed when the error occurred.
   * @param e      The description of the exception or error that occurred.
   */
  public FileProcessingException(String action, String e) {
    super("An error occurred while " + action + " file. " + e);
  }
}