package edu.ntnu.idatt2003.model.exceptions;

/**
 * <p>This exception is used to indicate that an input
 * expected to be a number is either missing or not in a valid numeric format.
 * The exception can be thrown to show errors during parsing
 * or validation of numeric fields in a user interface. </p>
 */
public class InvalidNumberException extends IllegalArgumentException {
  /**
   * Constructs an InvalidNumberException with a default
   * error message indicating that the input is not a number.
   */
  public InvalidNumberException() {
    super("Not a number");
  }

  /**
   * Constructs an InvalidNumberException with a custom error message.
   *
   * @param message The custom message that explains what caused the exception.
   */
  public InvalidNumberException(String message) {
    super(message);
  }

  /**
   * <p>Constructs an InvalidNumberException with a detailed message
   * that includes the parameter name and the expected type of number.
   * This constructor is particularly useful in contexts where
   * specific details about the input error are needed. </p>
   *
   * @param parameterName The name of the parameter that failed validation.
   * @param typeNumber The expected type of the number, such as "integer" or "double",
   *                  to provide more specific information about the expected format.
   */
  public InvalidNumberException(String parameterName, String typeNumber) {
    super("Required field for number is not a number. "
              + parameterName
              + " must be a "
              + typeNumber);
  }
}
