package edu.ntnu.idatt2003.model.exceptions;

/**
 * <p>Custom exception used to indicate that a
 * required field in a user interface is empty.
 * This exception is thrown to notify that a
 * required input field, expected to contain data, was left blank. </p>
 */
public class EmptyFieldException extends IllegalArgumentException {

  /**
   * Constructs an EmptyFieldException with a default
   * error message indicating that a required field is empty.
   */
  public EmptyFieldException() {

    super("Required field is empty.");
  }

}
