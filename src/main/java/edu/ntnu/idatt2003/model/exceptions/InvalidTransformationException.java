package edu.ntnu.idatt2003.model.exceptions;

/**
 * InvalidTransformationException extends IllegalArgumentException.
 * This class is a custom exception that extends {@link java.lang.IllegalArgumentException}.
 * Used to handle exceptions that occur when an invalid transformation is encountered.
 */
public class InvalidTransformationException extends IllegalArgumentException {

  /**
   * <p>Constructs a new InvalidTransformationException with a detailed error message.
   * The message should explain what aspect of the transformation was invalid.</p>
   *
   * @param e detailed message explaining what aspect of the transformation was invalid.
   */
  public InvalidTransformationException(String e) {
    super("Invalid transformation encountered: " + e);
  }
}
