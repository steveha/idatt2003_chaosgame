package edu.ntnu.idatt2003.model.transformations;

import edu.ntnu.idatt2003.model.math.Matrix2x2;
import edu.ntnu.idatt2003.model.math.Vector2D;
import edu.ntnu.idatt2003.util.ValidateParameters;

/**
 * <p>The AffineTransform2D class represents the affine transformation in a 2D space.
 * It implements the Transform2D interface and
 * provides methods to transform a point using the transformation.</p>
 */
public class AffineTransform2D implements Transform2D {
  private Matrix2x2 matrix;
  private Vector2D vector;

  /**
   * <p>Constructs a new AffineTransform2D object with a specified transformation matrix and vector.
   * This constructor initializes the transformation with the provided matrix and vector
   * for affine transformations by calling the {@code validateAffineParameters}
   * method from the ValidateParameters class.</p>
   *
   * @param matrix The transformation matrix used for affine transformations.
   * @param vector The vector used to translate the point after matrix multiplication.
   * @throws IllegalArgumentException if the matrix is not a 2x2 matrix.
   */
  public AffineTransform2D(Matrix2x2 matrix, Vector2D vector) {
    ValidateParameters.validateAffineParameters(matrix, vector);
    this.matrix = matrix;
    this.vector = vector;
  }

  /**
   * <p>Transforms the given point using this transformation.
   * Before performing the transformation, it validates that the point
   * is not null using the {@code validateVectorNotNull}
   * method from the ValidateParameters class.</p>
   *
   * @param point The Vector2D point to be transformed.
   * @return The transformed Vector2D point.
   * @throws NullPointerException if the input point is null.
   *
   */
  @Override
  public Vector2D transform(Vector2D point) {
    ValidateParameters.validateVectorNotNull(point);
    return matrix.multiply(point).add(vector);
  }

  /**
   * Returns the matrix of this transformation.
   *
   * @return the matrix
   */
  public Matrix2x2 getMatrix() {
    return matrix;
  }

  /**
   * Returns the vector of this transformation.
   *
   * @return the vector
   */
  public Vector2D getVector() {
    return vector;
  }
}