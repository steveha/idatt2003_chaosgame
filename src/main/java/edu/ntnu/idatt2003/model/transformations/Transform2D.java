package edu.ntnu.idatt2003.model.transformations;

import edu.ntnu.idatt2003.model.math.Vector2D;

/**
 * <p>Represents a 2D transformation, and is intended to be implemented by
 * transformation classes that manipulate points in 2D space.
 * Implementing this interface allows objects to
 * apply a specific transformation to a {@link Vector2D} instance</p>
 */
public interface Transform2D {
  Vector2D transform(Vector2D point);
}
