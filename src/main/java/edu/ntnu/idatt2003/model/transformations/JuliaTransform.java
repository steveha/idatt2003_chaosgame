package edu.ntnu.idatt2003.model.transformations;

import edu.ntnu.idatt2003.model.math.Complex;
import edu.ntnu.idatt2003.model.math.Vector2D;
import edu.ntnu.idatt2003.util.ValidateParameters;

/**
 * <p>The JuliaTransform class implements the Transform2D interface to define a transformation
 * used in the generation of Julia sets, and represents a Julia transformation in 2D space. </p>
 */
public class JuliaTransform implements Transform2D {
  private final Complex point;
  private final int sign;

  /**
   * <p>Constructs a new JuliaTransform object with a specified complex point and a sign.
   * It validates the sign to ensure it is correct using the {@code validateJuliaSign} method
   * from the ValidateParameters class.</p>
   *
   * @param point The complex point used for the transformation offset.
   * @param sign The sign used to determine the direction of the transformation.
   * @throws IllegalArgumentException if the sign is not +1 or -1.
   */
  public JuliaTransform(Complex point, int sign) {
    ValidateParameters.validateJuliaSign(sign);
    this.point = point;
    this.sign = sign;
  }

  /**
   * <p>Applies the Julia transformation to a given 2D point.
   * Before performing the transformation, it validates that the point
   * is not null using the {@code validateVectorNotNull}
   * method from the ValidateParameters class.</p>
   *
   * @param point The Vector2D point to be transformed.
   * @return The transformed Vector2D point representing the outcome of the Julia transformation.
   * @throws NullPointerException if the input point is null.
   */
  @Override
  public Vector2D transform(Vector2D point) {
    ValidateParameters.validateVectorNotNull(point);

    Vector2D pointSubtract = point.subtract(this.point);

    Complex complexShifted = new Complex(pointSubtract.getX0(), pointSubtract.getX1());
    Complex sqrtComplex = complexShifted.sqrt();

    double transformedX0 = sqrtComplex.getX0() * sign;
    double transformedX1 = sqrtComplex.getX1() * sign;

    return new Vector2D(transformedX0, transformedX1);
  }

  /**
   * Returns the point of this transformation.
   *
   * @return the point
   */
  public Complex getPoint() {
    return point;
  }
}