Affine2D # Type of transform
0, 0 # Lower left corner of the bounding box
1, 1 # Upper right corner of the bounding box
0.5, 0, 0, 0.5, 0, 0 # 1st transform parameters: scale by 0.5 without translation
0.5, 0, 0, 0.5, 0.25, 0.5 # 2nd transform parameters: scale by 0.5 and translate right
0.5, 0, 0, 0.5, 0.5, 0 # 3rd transform parameters: scale by 0.5 and translate up
