# IDATT2003_ChaosGame

<strong>STUDENT NAME</strong> = Hong An Ho <br>
<strong>STUDENT ID</strong> = 10026 <br>
<strong>STUDENT NAME</strong> = Steven Ha <br>
<strong>STUDENT ID</strong> = 10045


## Project description

<p> This Java project is a task assignment for the course <strong>IDATT2003 - Programmering 2  at NTNU.</strong></p>

The task is to develop a graphical application that visualizes fractals using a technique known as chaos games.
Users can create, view, and modify fractals like the Sierpinski triangle and the Julia set by adjusting parameters such as the number of iterations and the viewing scale.
The interface will be built using JavaFX.
The project is divided into three parts, providing step-by-step instructions for the application's development.

## Project structure

<p>The <strong>source</strong> files of this project is as follows:</p>

The main class is located in `src/main/java/edu/ntnu/idatt2003/ChaosGameApp.java`. <br>
Transformations are in `src/main/java/edu/ntnu/idatt2003/model/transformations` folder.<br>
Math logic is located in the `src/main/java/edu/ntnu/idatt2003/model/math` folder.<br>
Chaos classes are located in the `src/main/java/edu/ntnu/idatt2003/model/chaos` folder. <br>
While all the JUnit test classes are in the `src/test/java/edu/ntnu/idatt2003` folder.<br>

<p>The classes are organized into distinct packages for a more structured approach, which facilitates easier location of specific classes.
Additionally, the packages are named aptly to clearly reflect the contents and purpose of the classes they contain</p>


## Link to repository

For more information see [here](https://gitlab.stud.idi.ntnu.no/steveha/idatt2003_chaosgame).

## How to run the project

To run this project you can use the terminal or any IDE of your choice, such as IntelliJ IDEA or Eclipse.
<strong>Main</strong> is the main class in this project, and contains the main method.

For more information see [here](https://www.jetbrains.com/help/idea/run-java-applications.html).

## How to run the tests

<p>Before running tests <strong>JUnit 5</strong> needs to be installed in your IDE. Once this is installed in your choice of IDE, find the JUnit test classes within the project.</p>


After the test classes has been found, you can either run the whole test class or run the methods individually.
This is done by clicking the green play button next to the class or method name.
It will show you how many of the tests that has passed and failed,
which makes it easier to see if the tests are working as intended.
You can find the JUnit test classes in the `src/test/java/edu/ntnu/idatt2003` folder.

For more information see [here](https://www.jetbrains.com/help/idea/performing-tests.html).
